<?php
declare(strict_types=1);

namespace tagalink\service\segmentsBuilder;

final class
	SegmentsBuilder
{

	public static function build(string $path):array
	{
		if (mb_substr($path, 0, 1) == '/')
		{
			$path = mb_substr($path, 1);
		}

		if (mb_substr($path, -1) == '/')
		{
			$path = mb_substr(
				$path,
				0,
				mb_strlen($path) - 1
			);
		}

		$segments = explode(
			'/',
			$path
		);

		if ($segments[0] == '')
		{
			return [];
		}

		return $segments;
	}

}
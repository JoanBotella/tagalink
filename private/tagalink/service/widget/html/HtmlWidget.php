<?php
declare(strict_types=1);

namespace tagalink\service\widget\html;

final class
	HtmlWidget
{

	public static function render(HtmlWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/htmlWidgetTemplate.php');
		return ob_get_clean();
	}

}
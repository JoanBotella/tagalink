<?php
declare(strict_types=1);

/**
 * @var tagalink\service\widget\html\HtmlWidgetContext $context
 */

?>
<!DOCTYPE html>
<html lang="<?= $context->getLanguageCode() ?>" data-widget="html">
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<base href="<?= $context->getBase() ?>" />

	<title><?= $context->getTitle() ?></title>

<?php if ($context->hasCanonicalUrl()): ?>
	<link rel="canonical" href="<?= $context->getCanonicalUrlAfterHas() ?>" />

<?php endif ?>
	<link rel="icon" href="asset/image/favicon.png" type="image/png" />

	<link href="asset/style/tagalink.css" rel="stylesheet" media="screen" />

	<script src="asset/script/tagalink.js" defer="defer"></script>
</head>
<?= $context->getFrontBodyWidget() ?>
</html>
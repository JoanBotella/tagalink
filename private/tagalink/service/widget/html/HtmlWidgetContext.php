<?php
declare(strict_types=1);

namespace tagalink\service\widget\html;

final class
	HtmlWidgetContext
{

	private string $languageCode;
	private string $base;
	private string $title;
	private string $bodyWidget;
	private string $canonicalUrl;

	public function __construct(
		string $languageCode,
		string $base,
		string $title,
		string $bodyWidget
	)
	{
		$this->languageCode = $languageCode;
		$this->base = $base;
		$this->title = $title;
		$this->bodyWidget = $bodyWidget;
	}

	public function getLanguageCode():string
	{
		return $this->languageCode;
	}

	public function getBase():string
	{
		return $this->base;
	}

	public function getTitle():string
	{
		return $this->title;
	}

	public function getFrontBodyWidget():string
	{
		return $this->bodyWidget;
	}

	public function hasCanonicalUrl():bool
	{
		return isset($this->canonicalUrl);
	}

	public function getCanonicalUrlAfterHas():string
	{
		return $this->canonicalUrl;
	}

	public function setCanonicalUrl(string $v):void
	{
		$this->canonicalUrl = $v;
	}

	public function unsetCanonicalUrl():void
	{
		unset($this->canonicalUrl);
	}

}
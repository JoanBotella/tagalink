<?php
declare(strict_types=1);

namespace tagalink\service\router;

use Exception;
use tagalink\library\route\RouteItf;
use tagalink\service\configuration\Configuration;
use tagalink\service\pathContainer\PathContainer;

final class
	Router
{

	public static function route():void
	{
		$segments = PathContainer::getSegments();

		try
		{
			foreach (Configuration::getRoutes() as $route)
			{
				if (self::segmentsMatch($segments, $route))
				{
					self::runController($route);
					return;
				}
			}
		}
		catch (Exception $exception)
		{
			self::runInternalServerErrorController();
			return;
		}

		self::runNotFoundController();
	}

		private static function segmentsMatch(array $segments, RouteItf $route):bool
		{
			$segmentsMatcher = $route->buildSegmentsMatcher();
			return $segmentsMatcher->match($segments);
		}

		private static function runController(RouteItf $route):void
		{
			$controller = $route->buildController();
			$controller->run();
		}

		private static function runInternalServerErrorController():void
		{
			$internalServerErrorRoute = Configuration::getInternalServerErrorRoute();
			$internalServerErrorController = $internalServerErrorRoute->buildController();
			$internalServerErrorController->run();
		}

		private static function runNotFoundController():void
		{
			$notFoundRoute = Configuration::getNotFoundRoute();
			$notFoundController = $notFoundRoute->buildController();
			$notFoundController->run();
		}

}
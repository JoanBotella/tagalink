<?php
declare(strict_types=1);

namespace tagalink\service\translationContainer;

use Exception;
use tagalink\library\Constant;
use tagalink\library\translation\CaTranslation;
use tagalink\library\translation\EnTranslation;
use tagalink\library\translation\EsTranslation;
use tagalink\library\translation\TranslationItf;
use tagalink\service\languageCodeContainer\LanguageCodeContainer;

final class
	TranslationContainer
{

	private static array $translations;

	public static function get():TranslationItf
	{
		return self::getByLanguageCode(
			LanguageCodeContainer::get()
		);
	}

	public static function getByLanguageCode(string $languageCode):TranslationItf
	{
		if (!isset(self::$translations[$languageCode]))
		{
			if (!isset(self::$translations))
			{
				self::$translations = [];
			}

			self::$translations[$languageCode] = self::buildTranslation($languageCode);
		}
		return self::$translations[$languageCode];
	}

		private static function buildTranslation(string $languageCode):TranslationItf
		{
			return
				match ($languageCode)
				{
					Constant::LANGUAGE_CODE_CA => new CaTranslation(),
					Constant::LANGUAGE_CODE_EN => new EnTranslation(),
					Constant::LANGUAGE_CODE_ES => new EsTranslation(),
					default => throw new Exception('Language code '.$languageCode.' not supported'),
				}
			;
		}

}
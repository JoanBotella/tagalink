<?php
declare(strict_types=1);

namespace tagalink\service\absoluteUrlBuilder;

use tagalink\service\configuration\Configuration;

final class
	AbsoluteUrlBuilder
{

	public static function build(string $relativeUrl):string
	{
		return
			Configuration::getUrlProtocol()
			.'://'
			.Configuration::getDomain()
			.Configuration::getPathBase()
			.$relativeUrl
		;
	}

}
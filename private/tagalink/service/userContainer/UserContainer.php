<?php
declare(strict_types=1);

namespace tagalink\service\userContainer;

use Exception;
use tagalink\service\persister\user\UserModel;
use tagalink\service\persister\user\UserPersister;
use tagalink\service\sessionWrapper\SessionWrapper;

final class
	UserContainer
{

	private static ?UserModel $userModel;

	public static function has():bool
	{
		return
			isset(self::$userModel)
			|| SessionWrapper::hasUserId()
		;
	}

	public static function getAfterHas():UserModel
	{
		if (!isset(self::$userModel))
		{
			self::$userModel = self::fetchUserModel();
		}
		return self::$userModel;
	}

		private static function fetchUserModel():UserModel
		{
			$userId = SessionWrapper::getUserIdAfterHas();

			$userModels = UserPersister::selectById($userId);

			if (!$userModels)
			{
				throw new Exception('No user was found with id '.$userId);
			}

			if (count($userModels) > 1)
			{
				throw new Exception('More than one user was found with id '.$userId);
			}

			return reset($userModels);
		}

	public static function set(UserModel $userModel):void
	{
		self::$userModel = $userModel;
		SessionWrapper::setUserId(
			$userModel->getId()
		);
	}

	public static function unset():void
	{
		self::$userModel = null;
		SessionWrapper::unsetUserId();
	}

}
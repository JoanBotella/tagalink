<?php
declare(strict_types=1);

namespace tagalink\service\useCase\logout;

use tagalink\library\useCase\UseCaseAbs;
use tagalink\service\userContainer\UserContainer;

final class
	LogoutUseCase
extends
	UseCaseAbs
{
	public const ERROR_CODE_USER_NOT_LOGGED = 0;

	public static function run():void
	{
		static::reset();

		static::logout();
	}

		private static function logout():void
		{
			if (UserContainer::has())
			{
				UserContainer::unset();
				return;
			}
			static::addErrorCode(self::ERROR_CODE_USER_NOT_LOGGED);
		}

}
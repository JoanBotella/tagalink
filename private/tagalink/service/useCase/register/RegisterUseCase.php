<?php
declare(strict_types=1);

namespace tagalink\service\useCase\register;

use tagalink\library\Constant;
use tagalink\library\useCase\UseCaseAbs;
use tagalink\service\validator\Validator;

final class
	RegisterUseCase
extends
	UseCaseAbs
{
	public const ERROR_CODE_EMAIL_IS_NOT_VALID = 0;
	public const ERROR_CODE_EMAIL_IS_TOO_LONG = 1;
	public const ERROR_CODE_PASSWORD_IS_TOO_LONG = 2;
	public const ERROR_CODE_USERNAME_IS_TOO_LONG = 3;

	private static string $email;
	private static string $password;
	private static string $username;

	public static function run(string $email, string $password, string $username):void
	{
		static::reset();

		self::$email = $email;
		self::$password = $password;
		self::$username = $username;

		static::validateInput();

		if (static::hasErrorCodes())
		{
			return;
		}

		static::register();
	}

		private static function validateInput():void
		{
			if (mb_strlen(self::$email) > Constant::USER_EMAIL_SIZE)
			{
				static::addErrorCode(self::ERROR_CODE_EMAIL_IS_TOO_LONG);
			}

			if (!Validator::isEmail(self::$email))
			{
				static::addErrorCode(self::ERROR_CODE_EMAIL_IS_NOT_VALID);
			}

			if (mb_strlen(self::$password) > Constant::USER_PASSWORD_SIZE)
			{
				static::addErrorCode(self::ERROR_CODE_PASSWORD_IS_TOO_LONG);
			}

			if (mb_strlen(self::$username) > Constant::USER_USERNAME_SIZE)
			{
				static::addErrorCode(self::ERROR_CODE_USERNAME_IS_TOO_LONG);
			}
		}

		private static function register():void
		{
			// !!!
		}

}
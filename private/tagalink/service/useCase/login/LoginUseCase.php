<?php
declare(strict_types=1);

namespace tagalink\service\useCase\login;

use Exception;
use tagalink\library\Constant;
use tagalink\library\useCase\UseCaseAbs;
use tagalink\service\userContainer\UserContainer;
use tagalink\service\persister\user\UserPersister;
use tagalink\service\sessionWrapper\SessionWrapper;
use tagalink\service\validator\Validator;

final class
	LoginUseCase
extends
	UseCaseAbs
{
	public const ERROR_CODE_EMAIL_IS_NOT_VALID = 0;
	public const ERROR_CODE_EMAIL_IS_TOO_LONG = 1;
	public const ERROR_CODE_PASSWORD_IS_TOO_LONG = 2;
	public const ERROR_CODE_USER_NOT_FOUND = 3;

	private static string $email;
	private static string $password;

	public static function run(string $email, string $password):void
	{
		static::reset();

		self::$email = $email;
		self::$password = $password;

		static::validateInput();

		if (static::hasErrorCodes())
		{
			return;
		}

		static::login();
	}

		private static function validateInput():void
		{
			if (mb_strlen(self::$email) > Constant::USER_EMAIL_SIZE)
			{
				static::addErrorCode(self::ERROR_CODE_EMAIL_IS_TOO_LONG);
			}

			if (!Validator::isEmail(self::$email))
			{
				static::addErrorCode(self::ERROR_CODE_EMAIL_IS_NOT_VALID);
			}

			if (mb_strlen(self::$password) > Constant::USER_PASSWORD_SIZE)
			{
				static::addErrorCode(self::ERROR_CODE_PASSWORD_IS_TOO_LONG);
			}
		}

		private static function login():void
		{
			$userModels = UserPersister::selectByEmailAndPassword(
				self::$email,
				self::$password
			);

			if (!$userModels)
			{
				static::addErrorCode(self::ERROR_CODE_USER_NOT_FOUND);
				return;
			}

			if (count($userModels) > 1)
			{
				throw new Exception('More than one user was found with email '.self::$email);
			}

			UserContainer::set(
				reset($userModels)
			);
		}

}
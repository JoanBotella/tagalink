<?php
declare(strict_types=1);

namespace tagalink\service\notificationContainer;

use tagalink\service\sessionWrapper\SessionWrapper;

final class
	NotificationContainer
{

	public static function addError(string $error):void
	{
		$errors = self::getErrorsOrBuild();
		$errors[] = $error;
		SessionWrapper::setNotificationErrors($errors);
	}

		private static function getErrorsOrBuild():array
		{
			return
				SessionWrapper::hasNotificationErrors()
					? SessionWrapper::getNotificationErrorsAfterHas()
					: []
			;
		}

	public static function getErrorsAndUnset():array
	{
		$errors = self::getErrorsOrBuild();
		SessionWrapper::unsetNotificationErrors();
		return $errors;
	}

	public static function addWarning(string $warning):void
	{
		$warnings = self::getWarningsOrBuild();
		$warnings[] = $warning;
		SessionWrapper::setNotificationWarnings($warnings);
	}

		private static function getWarningsOrBuild():array
		{
			return
				SessionWrapper::hasNotificationWarnings()
					? SessionWrapper::getNotificationWarningsAfterHas()
					: []
			;
		}

	public static function getWarningsAndUnset():array
	{
		$warnings = self::getWarningsOrBuild();
		SessionWrapper::unsetNotificationWarnings();
		return $warnings;
	}

	public static function addSuccess(string $success):void
	{
		$successes = self::getSuccessesOrBuild();
		$successes[] = $success;
		SessionWrapper::setNotificationSuccesses($successes);
	}

		private static function getSuccessesOrBuild():array
		{
			return
				SessionWrapper::hasNotificationSuccesses()
					? SessionWrapper::getNotificationSuccessesAfterHas()
					: []
			;
		}

	public static function getSuccessesAndUnset():array
	{
		$successes = self::getSuccessesOrBuild();
		SessionWrapper::unsetNotificationSuccesses();
		return $successes;
	}

	public static function addInfo(string $info):void
	{
		$infos = self::getInfosOrBuild();
		$infos[] = $info;
		SessionWrapper::setNotificationInfos($infos);
	}

		private static function getInfosOrBuild():array
		{
			return
				SessionWrapper::hasNotificationInfos()
					? SessionWrapper::getNotificationInfosAfterHas()
					: []
			;
		}

	public static function getInfosAndUnset():array
	{
		$infos = self::getInfosOrBuild();
		SessionWrapper::unsetNotificationInfos();
		return $infos;
	}

}
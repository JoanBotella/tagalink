<?php
declare(strict_types=1);

namespace tagalink\service\pdoContainer;

use PDO;
use tagalink\service\configuration\Configuration;

final class
	PdoContainer
{

	private static PDO $pdo;

	public static function get():PDO
	{
		if (!isset(self::$pdo))
		{
			self::$pdo = self::buildPdo();
		}
		return self::$pdo;
	}

		private static function buildPdo():PDO
		{
			$dsn = 
				'mysql'
				.':host='.Configuration::getDatabaseHost()
				.';dbname='.Configuration::getDatabaseName()
			;
			return new PDO(
				$dsn,
				Configuration::getDatabaseUsername(),
				Configuration::getDatabasePassword(),
				[]
			);
		}

}
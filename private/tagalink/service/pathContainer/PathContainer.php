<?php
declare(strict_types=1);

namespace tagalink\service\pathContainer;

use tagalink\service\configuration\Configuration;
use tagalink\service\segmentsBuilder\SegmentsBuilder;

final class
	PathContainer
{

	private static string $path;
	private static array $segments;

	public static function getPath():string
	{
		if (!isset(self::$path))
		{
			self::$path = self::buildPath();
		}
		return self::$path;
	}

		private static function buildPath():string
		{
			return mb_substr(
				$_SERVER['REQUEST_URI'],
				mb_strlen(
					Configuration::getPathBase()
				)
			);
		}

	public static function getSegments():array
	{
		if (!isset(self::$segments))
		{
			self::$segments = self::buildSegments();
		}
		return self::$segments;
	}

		private static function buildSegments():array
		{
			return SegmentsBuilder::build(
				self::getPath()
			);
		}

}
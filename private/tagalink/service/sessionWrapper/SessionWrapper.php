<?php
declare(strict_types=1);

namespace tagalink\service\sessionWrapper;

use tagalink\library\sessionWrapper\SessionWrapperAbs;

final class
	SessionWrapper
extends
	SessionWrapperAbs
{

	// --- USER_ID ---

	private const KEY_USER_ID = 'user_id';
	private const KEY_NOTIFICATION_ERRORS = 'notification_errors';
	private const KEY_NOTIFICATION_WARNINGS = 'notification_warnings';
	private const KEY_NOTIFICATION_SUCCESSES = 'notification_successes';
	private const KEY_NOTIFICATION_INFOS = 'notification_infos';

	public static function hasUserId():bool
	{
		return static::has(self::KEY_USER_ID);
	}

	public static function getUserIdAfterHas():int
	{
		return static::getAfterHas(self::KEY_USER_ID);
	}

	public static function setUserId(int $userId):void
	{
		static::set(self::KEY_USER_ID, $userId);
	}

	public static function unsetUserId():void
	{
		static::unset(self::KEY_USER_ID);
	}

	public static function hasNotificationErrors():bool
	{
		return static::has(self::KEY_NOTIFICATION_ERRORS);
	}

	public static function getNotificationErrorsAfterHas():array
	{
		return static::getAfterHas(self::KEY_NOTIFICATION_ERRORS);
	}

	public static function setNotificationErrors(array $notificationErrors):void
	{
		static::set(self::KEY_NOTIFICATION_ERRORS, $notificationErrors);
	}

	public static function unsetNotificationErrors():void
	{
		static::unset(self::KEY_NOTIFICATION_ERRORS);
	}

	public static function hasNotificationWarnings():bool
	{
		return static::has(self::KEY_NOTIFICATION_WARNINGS);
	}

	public static function getNotificationWarningsAfterHas():array
	{
		return static::getAfterHas(self::KEY_NOTIFICATION_WARNINGS);
	}

	public static function setNotificationWarnings(array $notificationWarnings):void
	{
		static::set(self::KEY_NOTIFICATION_WARNINGS, $notificationWarnings);
	}

	public static function unsetNotificationWarnings():void
	{
		static::unset(self::KEY_NOTIFICATION_WARNINGS);
	}

	public static function hasNotificationSuccesses():bool
	{
		return static::has(self::KEY_NOTIFICATION_SUCCESSES);
	}

	public static function getNotificationSuccessesAfterHas():array
	{
		return static::getAfterHas(self::KEY_NOTIFICATION_SUCCESSES);
	}

	public static function setNotificationSuccesses(array $notificationSuccesses):void
	{
		static::set(self::KEY_NOTIFICATION_SUCCESSES, $notificationSuccesses);
	}

	public static function unsetNotificationSuccesses():void
	{
		static::unset(self::KEY_NOTIFICATION_SUCCESSES);
	}

	public static function hasNotificationInfos():bool
	{
		return static::has(self::KEY_NOTIFICATION_INFOS);
	}

	public static function getNotificationInfosAfterHas():array
	{
		return static::getAfterHas(self::KEY_NOTIFICATION_INFOS);
	}

	public static function setNotificationInfos(array $notificationInfos):void
	{
		static::set(self::KEY_NOTIFICATION_INFOS, $notificationInfos);
	}

	public static function unsetNotificationInfos():void
	{
		static::unset(self::KEY_NOTIFICATION_INFOS);
	}

}
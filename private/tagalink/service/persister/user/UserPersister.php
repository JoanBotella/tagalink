<?php
declare(strict_types=1);

namespace tagalink\service\persister\user;

use PDO;
use tagalink\library\persister\PersisterAbs;
use tagalink\service\pdoContainer\PdoContainer;

final class
	UserPersister
extends
	PersisterAbs
{

	private const TABLE_NAME = 'user';

	private const COLUMN_ID = 'id';
	private const COLUMN_EMAIL = 'email';
	private const COLUMN_USERNAME = 'username';
	private const COLUMN_PASSWORD = 'password';

	protected static function getTableName():string
	{
		return self::TABLE_NAME;
	}

	public static function selectById(int $id):array
	{
		$pdo = PdoContainer::get();

		$sql = '
SELECT
	*
FROM
	`'.static::getPrefixedTableName().'`
WHERE
	`'.self::COLUMN_ID.'` = '.$id.'
;
		';

		$rows = $pdo->query(
			$sql,
			PDO::FETCH_ASSOC
		);

		$result = [];

		foreach ($rows as $row)
		{
			$result[] = static::buildUserModelByRow($row);
		}

		return $result;
	}


		private static function buildUserModelByRow(array $row):UserModel
		{
			return new UserModel(
				(int) $row[self::COLUMN_ID],
				self::COLUMN_EMAIL,
				self::COLUMN_PASSWORD,
				self::COLUMN_USERNAME
			);
		}

	public static function selectByEmailAndPassword(string $email, string $password):array
	{
		$pdo = PdoContainer::get();

		$sql = '
SELECT
	*
FROM
	`'.static::getPrefixedTableName().'`
WHERE
	`'.self::COLUMN_EMAIL.'` = \''.$email.'\'
	AND `'.self::COLUMN_PASSWORD.'` = \''.$password.'\'
;
		';

		$rows = $pdo->query(
			$sql,
			PDO::FETCH_ASSOC
		);

		$result = [];

		foreach ($rows as $row)
		{
			$result[] = static::buildUserModelByRow($row);
		}

		return $result;
	}
	
}
<?php
declare(strict_types=1);

namespace tagalink\service\persister\user;

final class
	UserModel
{

	private int $id;
	private string $email;
	private string $password;
	private string $username;

	public function __construct(
		int $id,
		string $email,
		string $password,
		string $username
	)
	{
		$this->id = $id;
		$this->email = $email;
		$this->password = $password;
		$this->username = $username;
	}

	public function getId():int
	{
		return $this->id;
	}

	public function getEmail():string
	{
		return $this->email;
	}

	public function getPassword():string
	{
		return $this->password;
	}

	public function getUsername():string
	{
		return $this->username;
	}

}
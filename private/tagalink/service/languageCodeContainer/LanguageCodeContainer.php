<?php
declare(strict_types=1);

namespace tagalink\service\languageCodeContainer;

use tagalink\service\pathContainer\PathContainer;
use tagalink\service\configuration\Configuration;

final class
	LanguageCodeContainer
{

	private static string $languageCode;

	public static function get():string
	{
		if (!isset(self::$languageCode))
		{
			self::$languageCode = self::guess();
		}
		return self::$languageCode;
	}

		private static function guess():string
		{
			$segments = PathContainer::getSegments();
			if ($segments)
			{
				return reset($segments);
			}
			return Configuration::getDefaultLanguageCode();
		}

}
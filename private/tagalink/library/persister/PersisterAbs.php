<?php
declare(strict_types=1);

namespace tagalink\library\persister;

use tagalink\service\configuration\Configuration;

abstract class
	PersisterAbs
{

	protected static function getPrefixedTableName():string
	{
		return Configuration::getDatabasePrefix().static::getTableName();
	}

		abstract protected static function getTableName():string;

}
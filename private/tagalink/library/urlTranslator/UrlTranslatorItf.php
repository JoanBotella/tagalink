<?php
declare(strict_types=1);

namespace tagalink\library\urlTranslator;

interface
	UrlTranslatorItf
{

	public function translateByPath(string $languageCode, string $path):string;

	public function translateBySegments(string $languageCode, array $segments):string;

}
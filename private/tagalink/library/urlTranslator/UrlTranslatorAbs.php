<?php
declare(strict_types=1);

namespace tagalink\library\urlTranslator;

use tagalink\library\urlTranslator\UrlTranslatorItf;
use tagalink\service\segmentsBuilder\SegmentsBuilder;

abstract class
	UrlTranslatorAbs
implements
	UrlTranslatorItf
{

	public function translateByPath(string $languageCode, string $path):string
	{
		return
			$this->translateBySegments(
				$languageCode,
				SegmentsBuilder::build($path)
			)
		;
	}

	abstract public function translateBySegments(string $languageCode, array $segments):string;

}
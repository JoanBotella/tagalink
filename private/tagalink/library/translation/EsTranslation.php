<?php
declare(strict_types=1);

namespace tagalink\library\translation;

use tagalink\book\form\library\translation\FormEsTranslationTrait;
use tagalink\book\front\library\translation\FrontEsTranslationTrait;
use tagalink\library\translation\TranslationItf;

final class
	EsTranslation
implements
	TranslationItf
{
	use
		FrontEsTranslationTrait,
		FormEsTranslationTrait
	;

}

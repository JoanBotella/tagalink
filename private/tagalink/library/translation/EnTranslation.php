<?php
declare(strict_types=1);

namespace tagalink\library\translation;

use tagalink\book\form\library\translation\FormEnTranslationTrait;
use tagalink\book\front\library\translation\FrontEnTranslationTrait;
use tagalink\library\translation\TranslationItf;

final class
	EnTranslation
implements
	TranslationItf
{
	use
		FrontEnTranslationTrait,
		FormEnTranslationTrait
	;

}

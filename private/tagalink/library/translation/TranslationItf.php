<?php
declare(strict_types=1);

namespace tagalink\library\translation;

use tagalink\book\form\library\translation\FormTranslationItf;
use tagalink\book\front\library\translation\FrontTranslationItf;

interface
	TranslationItf
extends
	FrontTranslationItf,
	FormTranslationItf
{

}

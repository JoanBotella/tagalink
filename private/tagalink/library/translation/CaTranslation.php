<?php
declare(strict_types=1);

namespace tagalink\library\translation;

use tagalink\book\form\library\translation\FormCaTranslationTrait;
use tagalink\book\front\library\translation\FrontCaTranslationTrait;
use tagalink\library\translation\TranslationItf;

final class
	CaTranslation
implements
	TranslationItf
{
	use
		FrontCaTranslationTrait,
		FormCaTranslationTrait
	;

}

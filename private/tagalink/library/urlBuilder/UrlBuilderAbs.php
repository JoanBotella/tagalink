<?php
declare(strict_types=1);

namespace tagalink\library\urlBuilder;

use tagalink\library\translation\TranslationItf;
use tagalink\service\languageCodeContainer\LanguageCodeContainer;
use tagalink\service\translationContainer\TranslationContainer;

abstract class
	UrlBuilderAbs
{

	protected static function getLanguageCode():string
	{
		return LanguageCodeContainer::get();
	}

	protected static function getTranslationByLanguageCode(string $languageCode):TranslationItf
	{
		return TranslationContainer::getByLanguageCode($languageCode);
	}

}
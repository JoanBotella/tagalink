<?php
declare(strict_types=1);

namespace tagalink\library\route;

use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

interface
	RouteItf
{

	public function buildController():ControllerItf;

	public function buildSegmentsMatcher():SegmentsMatcherItf;

	public function buildUrlTranslator():UrlTranslatorItf;

}
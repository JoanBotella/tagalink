<?php
declare(strict_types=1);

namespace tagalink\library\useCase;

abstract class
	UseCaseAbs
{

	private static ?array $errorCodes;

	protected static function reset():void
	{
		self::$errorCodes = null;
	}

	protected static function addErrorCode(int $errorCode):void
	{
		self::setupErrorCodesIfRequired();
		self::$errorCodes[] = $errorCode;
	}

		private static function setupErrorCodesIfRequired():void
		{
			if (!isset(self::$errorCodes))
			{
				self::$errorCodes = [];
			}
		}

	public static function hasErrorCodes():bool
	{
		return
			isset(self::$errorCodes)
			&& self::$errorCodes
		;
	}

	public static function getErrorCodesAfterHas():array
	{
		self::setupErrorCodesIfRequired();
		return self::$errorCodes;
	}

}
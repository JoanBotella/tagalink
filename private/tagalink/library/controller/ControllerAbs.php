<?php
declare(strict_types=1);

namespace tagalink\library\controller;

use Exception;
use tagalink\library\controller\ControllerItf;

abstract class
	ControllerAbs
implements
	ControllerItf
{

	public function run():void
	{
		$this->validateRequest();

		$this->runUseCase();

		$this->respond();
	}

		protected function validateRequest():void
		{
			if (!$this->isRequestValid())
			{
				throw new Exception('Request not valid');
			}
		}

			protected function isRequestValid():bool
			{
				return true;
			}

		protected function runUseCase():void
		{

		}

		abstract protected function respond():void;

}
<?php
declare(strict_types=1);

namespace tagalink\library\controller;

interface
	ControllerItf
{

	public function run():void;

}
<?php
declare(strict_types=1);

namespace tagalink\library\sessionWrapper;

abstract class
	SessionWrapperAbs
{

	private static bool $isSessionStarted;

	protected static function has(string $key):bool
	{
		self::startSessionIfRequired();
		return isset($_SESSION[$key]);
	}

		private static function startSessionIfRequired():void
		{
			if (!isset(self::$isSessionStarted))
			{
				session_start();
				self::$isSessionStarted = true;
			}
		}

	protected static function getAfterHas(string $key):mixed
	{
		self::startSessionIfRequired();
		return $_SESSION[$key];
	}

	protected static function set(string $key, mixed $value):void
	{
		self::startSessionIfRequired();
		$_SESSION[$key] = $value;
	}

	protected static function unset(string $key):void
	{
		self::startSessionIfRequired();
		unset($_SESSION[$key]);
	}

}
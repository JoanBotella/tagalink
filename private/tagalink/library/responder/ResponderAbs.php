<?php
declare(strict_types=1);

namespace tagalink\library\responder;

use tagalink\library\Constant;

abstract class
	ResponderAbs
{

	public static function run():void
	{
		self::setHttpHeaders();

		$body = static::buildBody();
		if ($body !== '')
		{
			echo $body;
		}
	}

		private static function setHttpHeaders():void
		{
			self::setHttpStatusIfRequired();

			foreach (static::buildHeaders() as $header)
			{
				header($header);
			}
		}

			private static function setHttpStatusIfRequired():void
			{
				$httpStatus = static::buildHttpStatus();
				if ($httpStatus !== Constant::HTTP_STATUS_OK)
				{
					http_response_code($httpStatus);
				}
			}

				protected static function buildHttpStatus():int
				{
					return Constant::HTTP_STATUS_OK;
				}

			protected static function buildHeaders():array
			{
				return [];
			}

		protected static function buildBody():string
		{
			return '';
		}

}

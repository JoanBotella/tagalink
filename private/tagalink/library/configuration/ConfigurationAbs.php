<?php
declare(strict_types=1);

namespace tagalink\library\configuration;

use tagalink\book\form\page\login\library\route\FormLoginRoute;
use tagalink\book\form\page\logout\library\route\FormLogoutRoute;
use tagalink\book\form\page\register\library\route\FormRegisterRoute;
use tagalink\library\language\CaLanguage;
use tagalink\library\language\EnLanguage;
use tagalink\library\language\EsLanguage;
use tagalink\library\route\RouteItf;
use tagalink\book\front\page\home\library\route\FrontHomeRoute;
use tagalink\book\front\page\login\library\route\FrontLoginRoute;
use tagalink\book\front\page\register\library\route\FrontRegisterRoute;
use tagalink\book\front\page\status400\library\route\FrontStatus400Route;
use tagalink\book\front\page\status404\library\route\FrontStatus404Route;
use tagalink\library\Constant;

abstract class
	ConfigurationAbs
{
	private static array $supportedLanguages;
	private static array $routes;

	public static function getSupportedLanguages():array
	{
		if (!isset(self::$supportedLanguages))
		{
			self::$supportedLanguages = static::buildSupportedLanguages();
		}
		return self::$supportedLanguages;
	}

		protected static function buildSupportedLanguages():array
		{
			return [
				new EnLanguage(),
				new EsLanguage(),
				new CaLanguage(),
			];
		}

	public static function getDefaultLanguageCode():string
	{
		return self::getSupportedLanguages()[0]->getCode();
	}

	public static function getRoutes():array
	{
		if (!isset(self::$routes))
		{
			self::$routes = static::buildRoutes();
		}
		return self::$routes;
	}

		protected static function buildRoutes():array
		{
			return [
				new FrontHomeRoute(),
				new FrontLoginRoute(),
				new FrontRegisterRoute(),
				new FrontStatus404Route(),
				new FrontStatus400Route(),
				new FormLoginRoute(),
				new FormLogoutRoute(),
				new FormRegisterRoute(),
			];
		}

	public static function getNotFoundRoute():RouteItf
	{
		return self::getRoutes()[1];
	}

	public static function getInternalServerErrorRoute():RouteItf
	{
		return self::getRoutes()[2];
	}

	abstract public static function getUrlProtocol():string;

	abstract public static function getDomain():string;

	abstract public static function getPathBase():string;

	abstract public static function getDatabaseHost():string;

	abstract public static function getDatabaseName():string;

	abstract public static function getDatabasePort():int;

	abstract public static function getDatabaseUsername():string;

	abstract public static function getDatabasePassword():string;

	abstract public static function getDatabasePrefix():string;

}

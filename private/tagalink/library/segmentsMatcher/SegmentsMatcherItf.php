<?php
declare(strict_types=1);

namespace tagalink\library\segmentsMatcher;

interface
	SegmentsMatcherItf
{

	public function match(array $segments):bool;

}
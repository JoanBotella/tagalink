<?php
declare(strict_types=1);

namespace tagalink\library\segmentsMatcher;

use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\translation\TranslationItf;
use tagalink\service\translationContainer\TranslationContainer;

abstract class
	SegmentsMatcherAbs
implements
	SegmentsMatcherItf
{

	protected function getTranslation():TranslationItf
	{
		return TranslationContainer::get();
	}

}
<?php
declare(strict_types=1);

namespace tagalink\library\language;

use tagalink\library\Constant;
use tagalink\library\language\LanguageItf;

class
	EnLanguage
implements
	LanguageItf
{

	public function getCode():string
	{
		return Constant::LANGUAGE_CODE_EN;
	}

	public function getName():string
	{
		return 'English';
	}

	public function getEnglishName():string
	{
		return 'English';
	}

}
<?php
declare(strict_types=1);

namespace tagalink\library\language;

interface
	LanguageItf
{

	public function getCode():string;

	public function getName():string;

	public function getEnglishName():string;

}

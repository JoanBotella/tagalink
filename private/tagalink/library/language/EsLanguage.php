<?php
declare(strict_types=1);

namespace tagalink\library\language;

use tagalink\library\Constant;
use tagalink\library\language\LanguageItf;

class
	EsLanguage
implements
	LanguageItf
{

	public function getCode():string
	{
		return Constant::LANGUAGE_CODE_ES;
	}

	public function getName():string
	{
		return 'Español';
	}

	public function getEnglishName():string
	{
		return 'Spanish';
	}

}
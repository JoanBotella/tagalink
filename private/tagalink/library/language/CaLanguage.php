<?php
declare(strict_types=1);

namespace tagalink\library\language;

use tagalink\library\Constant;
use tagalink\library\language\LanguageItf;

class
	CaLanguage
implements
	LanguageItf
{

	public function getCode():string
	{
		return Constant::LANGUAGE_CODE_CA;
	}

	public function getName():string
	{
		return 'Català';
	}

	public function getEnglishName():string
	{
		return 'Catalan';
	}

}
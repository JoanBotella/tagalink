<?php
declare(strict_types=1);

namespace tagalink\library;

final class
	Constant
{

	public const FORM_LOGIN_EMAIL_NAME = 'email';
	public const FORM_LOGIN_PASSWORD_NAME = 'password';

	public const FORM_REGISTER_EMAIL_NAME = 'email';
	public const FORM_REGISTER_PASSWORD_NAME = 'password';
	public const FORM_REGISTER_USERNAME_NAME = 'username';

	public const LANGUAGE_CODE_EN = 'en';
	public const LANGUAGE_CODE_ES = 'es';
	public const LANGUAGE_CODE_CA = 'ca';

	public const HTTP_STATUS_OK = 200;
	public const HTTP_STATUS_SEE_OTHER = 303;
	public const HTTP_STATUS_INTERNAL_SERVER_ERROR = 400;
	public const HTTP_STATUS_NOT_FOUND = 404;

	public const URL_PROTOCOL_HTTP = 'http';
	public const URL_PROTOCOL_HTTPS = 'https';

	public const USER_EMAIL_SIZE = 100;
	public const USER_PASSWORD_SIZE = 50;
	public const USER_USERNAME_SIZE = 50;

}
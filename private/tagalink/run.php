<?php
declare(strict_types=1);

use tagalink\service\router\Router;

require __DIR__.'/../composer/vendor/autoload.php';

Router::route();
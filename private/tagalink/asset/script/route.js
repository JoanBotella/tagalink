
const route = function ()
{
	const body = document.querySelector('body');

	if (body === undefined)
	{
		return;
	}

	const bookCode = body.getAttribute('data-book');

	if (bookCode === undefined)
	{
		throw 'data-book attribute not found!';
	}

	if (typeof bookSetuppers[bookCode] === 'function')
	{
		bookSetuppers[bookCode]();
	}

	const pageCode = body.getAttribute('data-page');

	if (pageCode === undefined)
	{
		throw 'data-page attribute not found!';
	}

	if (typeof pageSetuppers[pageCode] === 'function')
	{
		pageSetuppers[pageCode]();
	}
}
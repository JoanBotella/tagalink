<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\service\urlBuilder;

use tagalink\book\front\library\urlBuilder\FrontUrlBuilderAbs;

final class
	FrontRegisterUrlBuilder
extends
	FrontUrlBuilderAbs
{

	public static function build():string
	{
		return self::buildByLanguageCode(
			static::getLanguageCode()
		);
	}

	public static function buildByLanguageCode(string $languageCode):string
	{
		$translation = static::getTranslationByLanguageCode($languageCode);
		return
			$languageCode
			.'/'.$translation::pageFrontRegister_segment()
		;
	}

}
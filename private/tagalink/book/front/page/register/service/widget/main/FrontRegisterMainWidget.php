<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\service\widget\main;

final class
	FrontRegisterMainWidget
{

	public static function render(FrontRegisterMainWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontRegisterMainWidgetTemplate.php');
		return ob_get_clean();
	}

}
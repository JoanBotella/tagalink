<?php
declare(strict_types=1);

use tagalink\library\Constant;

/**
 * @var tagalink\book\front\page\register\service\widget\main\FrontRegisterMainWidgetContext $context
 */

$translation = $context->getTranslation();

?>
		<main data-widget="front-register-main">
			<h1><?= $translation->widgetFrontRegisterMain_h1() ?></h1>

			<form method="post" action="<?= $context->getActionUrl() ?>">
				<label>
					<span><?= $translation->widgetFrontRegisterMain_emailLabel() ?></span>
					<input name="<?= Constant::FORM_REGISTER_EMAIL_NAME ?>" type="email" required="required" maxlength="<?= Constant::USER_EMAIL_SIZE ?>" />
				</label>
				<label>
					<span><?= $translation->widgetFrontRegisterMain_passwordLabel() ?></span>
					<input name="<?= Constant::FORM_REGISTER_PASSWORD_NAME ?>" type="password" required="required" maxlength="<?= Constant::USER_PASSWORD_SIZE ?>" />
				</label>
				<label>
					<span><?= $translation->widgetFrontRegisterMain_usernameLabel() ?></span>
					<input name="<?= Constant::FORM_REGISTER_USERNAME_NAME ?>" required="required" maxlength="<?= Constant::USER_USERNAME_SIZE ?>" />
				</label>

				<input type="submit" value="<?= $translation->widgetFrontRegisterMain_submitValue() ?>" />
			</form>
		</main>

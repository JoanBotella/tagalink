<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\service\responder;

use tagalink\book\form\page\register\service\urlBuilder\FormRegisterUrlBuilder;
use tagalink\book\front\library\responder\FrontResponderAbs;
use tagalink\book\front\page\register\service\urlBuilder\FrontRegisterUrlBuilder;
use tagalink\book\front\page\register\service\widget\main\FrontRegisterMainWidget;
use tagalink\book\front\page\register\service\widget\main\FrontRegisterMainWidgetContext;

final class
	FrontRegisterResponder
extends
	FrontResponderAbs
{

	protected static function buildMainWidget():string
	{
		return FrontRegisterMainWidget::render(
			new FrontRegisterMainWidgetContext(
				static::getTranslation(),
				FormRegisterUrlBuilder::build()
			)
		);
	}

	protected static function getDataPage():string
	{
		return 'register';
	}

	protected static function buildCanonicalUrl():string
	{
		return FrontRegisterUrlBuilder::build();
	}

	protected static function getTitle():string
	{
		$translation = static::getTranslation();
		return $translation::pageFrontRegister_title();
	}

}
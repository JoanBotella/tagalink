<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\library\segmentsMatcher;

use tagalink\book\front\library\segmentsMatcher\FrontSegmentsMatcherAbs;

final class
	FrontRegisterSegmentsMatcher
extends
	FrontSegmentsMatcherAbs
{

	public function match(array $segments):bool
	{
		if (count($segments) != 2)
		{
			return false;
		}

		$translation = $this->getTranslation();
		return $segments[1] == $translation::pageFrontRegister_segment();
	}

}
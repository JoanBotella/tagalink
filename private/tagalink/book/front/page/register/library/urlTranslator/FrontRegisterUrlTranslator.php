<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\library\urlTranslator;

use tagalink\book\front\library\urlTranslator\FrontUrlTranslatorAbs;
use tagalink\book\front\page\register\service\urlBuilder\FrontRegisterUrlBuilder;

final class
	FrontRegisterUrlTranslator
extends
	FrontUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FrontRegisterUrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
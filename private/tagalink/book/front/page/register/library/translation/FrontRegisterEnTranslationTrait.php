<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\library\translation;

trait
	FrontRegisterEnTranslationTrait
{

	// --- pageFrontRegister ---

	public static function pageFrontRegister_title():string { return 'Register'; }

	public static function pageFrontRegister_segment():string { return 'register'; }

	// --- widgetFrontRegisterMain ---

	public static function widgetFrontRegisterMain_h1():string { return self::pageFrontRegister_title(); }

	public static function widgetFrontRegisterMain_emailLabel():string { return 'E-mail'; }

	public static function widgetFrontRegisterMain_passwordLabel():string { return 'Password'; }

	public static function widgetFrontRegisterMain_submitValue():string { return 'Enter'; }

	public static function widgetFrontRegisterMain_usernameLabel():string { return 'Username'; }

}
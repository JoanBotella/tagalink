<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\library\translation;

trait
	FrontRegisterCaTranslationTrait
{

	// --- pageFrontRegister ---

	public static function pageFrontRegister_title():string { return 'Registre'; }

	public static function pageFrontRegister_segment():string { return 'registre'; }

	// --- widgetFrontRegisterMain ---

	public static function widgetFrontRegisterMain_h1():string { return self::pageFrontRegister_title(); }

	public static function widgetFrontRegisterMain_emailLabel():string { return 'E-mail'; }

	public static function widgetFrontRegisterMain_passwordLabel():string { return 'Contrasenya'; }

	public static function widgetFrontRegisterMain_submitValue():string { return 'Entrar'; }

	public static function widgetFrontRegisterMain_usernameLabel():string { return 'Nom d\'usuari'; }

}
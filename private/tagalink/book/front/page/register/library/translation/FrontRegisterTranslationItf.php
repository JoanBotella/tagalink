<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\library\translation;

interface
	FrontRegisterTranslationItf
{

	// --- pageFrontRegister ---

	public static function pageFrontRegister_title():string;

	public static function pageFrontRegister_segment():string;

	// --- widgetFrontRegisterMain ---

	public static function widgetFrontRegisterMain_h1():string;

	public static function widgetFrontRegisterMain_emailLabel():string;

	public static function widgetFrontRegisterMain_passwordLabel():string;

	public static function widgetFrontRegisterMain_submitValue():string;

	public static function widgetFrontRegisterMain_usernameLabel():string;

}
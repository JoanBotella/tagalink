<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\front\page\register\library\controller\FrontRegisterController;
use tagalink\book\front\page\register\library\segmentsMatcher\FrontRegisterSegmentsMatcher;
use tagalink\book\front\page\register\library\urlTranslator\FrontRegisterUrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FrontRegisterRoute
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FrontRegisterController();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FrontRegisterSegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FrontRegisterUrlTranslator();
	}

}
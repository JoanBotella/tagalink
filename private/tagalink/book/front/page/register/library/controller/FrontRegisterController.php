<?php
declare(strict_types=1);

namespace tagalink\book\front\page\register\library\controller;

use tagalink\book\front\library\controller\FrontControllerAbs;
use tagalink\book\front\page\register\service\responder\FrontRegisterResponder;

final class
	FrontRegisterController
extends
	FrontControllerAbs
{

	protected function respond():void
	{
		FrontRegisterResponder::run();
	}

}
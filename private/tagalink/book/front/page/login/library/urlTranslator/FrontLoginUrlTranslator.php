<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\library\urlTranslator;

use tagalink\book\front\library\urlTranslator\FrontUrlTranslatorAbs;
use tagalink\book\front\page\login\service\urlBuilder\FrontLoginUrlBuilder;

final class
	FrontLoginUrlTranslator
extends
	FrontUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FrontLoginUrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
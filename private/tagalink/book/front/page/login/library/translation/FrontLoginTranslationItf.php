<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\library\translation;

interface
	FrontLoginTranslationItf
{

	// --- pageFrontLogin ---

	public static function pageFrontLogin_title():string;

	public static function pageFrontLogin_segment():string;

	// --- widgetFrontLoginMain ---

	public static function widgetFrontLoginMain_h1():string;

	public static function widgetFrontLoginMain_emailLabel():string;

	public static function widgetFrontLoginMain_passwordLabel():string;

	public static function widgetFrontLoginMain_submitValue():string;

}
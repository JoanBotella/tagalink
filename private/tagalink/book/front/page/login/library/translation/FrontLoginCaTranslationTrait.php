<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\library\translation;

trait
	FrontLoginCaTranslationTrait
{

	// --- pageFrontLogin ---

	public static function pageFrontLogin_title():string { return 'Entrar'; }

	public static function pageFrontLogin_segment():string { return 'entrar'; }

	// --- widgetFrontLoginMain ---

	public static function widgetFrontLoginMain_h1():string { return self::pageFrontLogin_title(); }

	public static function widgetFrontLoginMain_emailLabel():string { return 'E-mail'; }

	public static function widgetFrontLoginMain_passwordLabel():string { return 'Contrasenya'; }

	public static function widgetFrontLoginMain_submitValue():string { return 'Entrar'; }

}
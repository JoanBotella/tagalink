<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\library\segmentsMatcher;

use tagalink\book\front\library\segmentsMatcher\FrontSegmentsMatcherAbs;

final class
	FrontLoginSegmentsMatcher
extends
	FrontSegmentsMatcherAbs
{

	public function match(array $segments):bool
	{
		if (count($segments) != 2)
		{
			return false;
		}

		$translation = $this->getTranslation();
		return $segments[1] == $translation::pageFrontLogin_segment();
	}

}
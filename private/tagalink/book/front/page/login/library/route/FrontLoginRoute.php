<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\front\page\login\library\controller\FrontLoginController;
use tagalink\book\front\page\login\library\segmentsMatcher\FrontLoginSegmentsMatcher;
use tagalink\book\front\page\login\library\urlTranslator\FrontLoginUrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FrontLoginRoute
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FrontLoginController();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FrontLoginSegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FrontLoginUrlTranslator();
	}

}
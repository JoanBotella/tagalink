<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\library\controller;

use tagalink\book\front\library\controller\FrontControllerAbs;
use tagalink\book\front\page\login\service\responder\FrontLoginResponder;

final class
	FrontLoginController
extends
	FrontControllerAbs
{

	protected function respond():void
	{
		FrontLoginResponder::run();
	}

}
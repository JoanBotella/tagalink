<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\service\widget\main;

final class
	FrontLoginMainWidget
{

	public static function render(FrontLoginMainWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontLoginMainWidgetTemplate.php');
		return ob_get_clean();
	}

}
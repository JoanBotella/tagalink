<?php
declare(strict_types=1);

use tagalink\library\Constant;

/**
 * @var tagalink\book\front\page\login\service\widget\main\FrontLoginMainWidgetContext $context
 */

$translation = $context->getTranslation();

?>
		<main data-widget="front-login-main">
			<h1><?= $translation->widgetFrontLoginMain_h1() ?></h1>

			<form method="post" action="<?= $context->getActionUrl() ?>">
				<label>
					<span><?= $translation->widgetFrontLoginMain_emailLabel() ?></span>
					<input name="<?= Constant::FORM_LOGIN_EMAIL_NAME ?>" type="email" required="required" maxlength="<?= Constant::USER_EMAIL_SIZE ?>" />
				</label>
				<label>
					<span><?= $translation->widgetFrontLoginMain_passwordLabel() ?></span>
					<input name="<?= Constant::FORM_LOGIN_PASSWORD_NAME ?>" type="password" required="required" maxlength="<?= Constant::USER_PASSWORD_SIZE ?>" />
				</label>

				<input type="submit" value="<?= $translation->widgetFrontLoginMain_submitValue() ?>" />
			</form>
		</main>

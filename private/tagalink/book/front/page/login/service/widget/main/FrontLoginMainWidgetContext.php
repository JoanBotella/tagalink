<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\service\widget\main;

use tagalink\library\translation\TranslationItf;

final class
	FrontLoginMainWidgetContext
{

	private TranslationItf $translation;
	private string $actionUrl;

	public function __construct(
		TranslationItf $translation,
		string $actionUrl
	)
	{
		$this->translation = $translation;
		$this->actionUrl = $actionUrl;
	}

	public function getTranslation():TranslationItf
	{
		return $this->translation;
	}

	public function getActionUrl():string
	{
		return $this->actionUrl;
	}

}
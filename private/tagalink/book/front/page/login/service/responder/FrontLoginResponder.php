<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\service\responder;

use tagalink\book\form\page\login\service\urlBuilder\FormLoginUrlBuilder;
use tagalink\book\front\library\responder\FrontResponderAbs;
use tagalink\book\front\page\login\service\urlBuilder\FrontLoginUrlBuilder;
use tagalink\book\front\page\login\service\widget\main\FrontLoginMainWidget;
use tagalink\book\front\page\login\service\widget\main\FrontLoginMainWidgetContext;

final class
	FrontLoginResponder
extends
	FrontResponderAbs
{

	protected static function buildMainWidget():string
	{
		return FrontLoginMainWidget::render(
			new FrontLoginMainWidgetContext(
				static::getTranslation(),
				FormLoginUrlBuilder::build()
			)
		);
	}

	protected static function getDataPage():string
	{
		return 'login';
	}

	protected static function buildCanonicalUrl():string
	{
		return FrontLoginUrlBuilder::build();
	}

	protected static function getTitle():string
	{
		$translation = static::getTranslation();
		return $translation::pageFrontLogin_title();
	}

}
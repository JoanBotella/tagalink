<?php
declare(strict_types=1);

namespace tagalink\book\front\page\login\service\urlBuilder;

use tagalink\book\front\library\urlBuilder\FrontUrlBuilderAbs;

final class
	FrontLoginUrlBuilder
extends
	FrontUrlBuilderAbs
{

	public static function build():string
	{
		return self::buildByLanguageCode(
			static::getLanguageCode()
		);
	}

	public static function buildByLanguageCode(string $languageCode):string
	{
		$translation = static::getTranslationByLanguageCode($languageCode);
		return
			$languageCode
			.'/'.$translation::pageFrontLogin_segment()
		;
	}

}
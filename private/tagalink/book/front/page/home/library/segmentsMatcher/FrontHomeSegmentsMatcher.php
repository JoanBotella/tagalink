<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\segmentsMatcher;

use tagalink\book\front\library\segmentsMatcher\FrontSegmentsMatcherAbs;

final class
	FrontHomeSegmentsMatcher
extends
	FrontSegmentsMatcherAbs
{

	public function match(array $segments):bool
	{
		$segmentsCount = count($segments);

		if ($segmentsCount < 2)
		{
			return true;
		}

		if ($segmentsCount > 2)
		{
			return false;
		}

		$translation = $this->getTranslation();
		return $segments[1] == $translation::pageFrontHome_segment();
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\front\page\home\library\controller\FrontHomeController;
use tagalink\book\front\page\home\library\segmentsMatcher\FrontHomeSegmentsMatcher;
use tagalink\book\front\page\home\library\urlTranslator\FrontHomeUrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FrontHomeRoute
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FrontHomeController();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FrontHomeSegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FrontHomeUrlTranslator();
	}

}
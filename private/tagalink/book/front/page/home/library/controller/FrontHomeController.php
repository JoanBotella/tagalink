<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\controller;

use tagalink\book\front\library\controller\FrontControllerAbs;
use tagalink\book\front\page\home\service\responder\FrontHomeResponder;

final class
	FrontHomeController
extends
	FrontControllerAbs
{

	protected function respond():void
	{
		FrontHomeResponder::run();
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\translation;

interface
	FrontHomeTranslationItf
{

	// --- pageFrontHome ---

	public static function pageFrontHome_title():string;

	public static function pageFrontHome_segment():string;

	// --- widgetFrontHomeMain ---

	public static function widgetFrontHomeMain_h1():string;

	public static function widgetFrontHomeMain_message():string;

}
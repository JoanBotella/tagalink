<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\translation;

trait
	FrontHomeEnTranslationTrait
{

	// --- pageFrontHome ---

	public static function pageFrontHome_title():string { return 'Home'; }

	public static function pageFrontHome_segment():string { return 'home'; }

	// --- widgetFrontHomeMain ---

	public static function widgetFrontHomeMain_h1():string { return self::pageFrontHome_title(); }

	public static function widgetFrontHomeMain_message():string { return 'This is the '.self::pageFrontHome_title().' page.'; }

}
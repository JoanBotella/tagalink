<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\translation;

trait
	FrontHomeCaTranslationTrait
{

	// --- pageFrontHome ---

	public static function pageFrontHome_title():string { return 'Inici'; }

	public static function pageFrontHome_segment():string { return 'inici'; }

	// --- widgetFrontHomeMain ---

	public static function widgetFrontHomeMain_h1():string { return self::pageFrontHome_title(); }

	public static function widgetFrontHomeMain_message():string { return 'Ésta és la pàgina '.self::pageFrontHome_title().'.'; }

}
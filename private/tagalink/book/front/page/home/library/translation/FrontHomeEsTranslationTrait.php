<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\translation;

trait
	FrontHomeEsTranslationTrait
{

	// --- pageFrontHome ---

	public static function pageFrontHome_title():string { return 'Inicio'; }

	public static function pageFrontHome_segment():string { return 'inicio'; }

	// --- widgetFrontHomeMain ---

	public static function widgetFrontHomeMain_h1():string { return self::pageFrontHome_title(); }

	public static function widgetFrontHomeMain_message():string { return 'Ésta es la página '.self::pageFrontHome_title().'.'; }

}
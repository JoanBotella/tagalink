<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\library\urlTranslator;

use tagalink\book\front\library\urlTranslator\FrontUrlTranslatorAbs;
use tagalink\book\front\page\home\service\urlBuilder\FrontHomeUrlBuilder;

final class
	FrontHomeUrlTranslator
extends
	FrontUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FrontHomeUrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
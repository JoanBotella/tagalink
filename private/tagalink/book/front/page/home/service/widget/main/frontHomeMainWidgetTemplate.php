<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\page\home\service\widget\main\FrontHomeMainWidgetContext $context
 */

$translation = $context->getTranslation();

?>
		<main data-widget="front-home-main">
			<h1><?= $translation->widgetFrontHomeMain_h1() ?></h1>

			<p><?= $translation->widgetFrontHomeMain_message() ?></p>
		</main>

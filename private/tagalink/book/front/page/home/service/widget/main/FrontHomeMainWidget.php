<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\service\widget\main;

final class
	FrontHomeMainWidget
{

	public static function render(FrontHomeMainWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontHomeMainWidgetTemplate.php');
		return ob_get_clean();
	}

}
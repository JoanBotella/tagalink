<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\service\widget\main;

use tagalink\library\translation\TranslationItf;

final class
	FrontHomeMainWidgetContext
{

	private TranslationItf $translation;

	public function __construct(
		TranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():TranslationItf
	{
		return $this->translation;
	}

}
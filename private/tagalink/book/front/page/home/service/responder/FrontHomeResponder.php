<?php
declare(strict_types=1);

namespace tagalink\book\front\page\home\service\responder;

use tagalink\book\front\library\responder\FrontResponderAbs;
use tagalink\book\front\page\home\service\urlBuilder\FrontHomeUrlBuilder;
use tagalink\book\front\page\home\service\widget\main\FrontHomeMainWidget;
use tagalink\book\front\page\home\service\widget\main\FrontHomeMainWidgetContext;

final class
	FrontHomeResponder
extends
	FrontResponderAbs
{

	protected static function buildMainWidget():string
	{
		return FrontHomeMainWidget::render(
			new FrontHomeMainWidgetContext(
				static::getTranslation()
			)
		);
	}

	protected static function getDataPage():string
	{
		return 'home';
	}

	protected static function buildCanonicalUrl():string
	{
		return FrontHomeUrlBuilder::build();
	}

	protected static function getTitle():string
	{
		$translation = static::getTranslation();
		return $translation::pageFrontHome_title();
	}

}
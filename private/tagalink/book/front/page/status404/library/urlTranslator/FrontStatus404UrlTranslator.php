<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\library\urlTranslator;

use tagalink\book\front\library\urlTranslator\FrontUrlTranslatorAbs;
use tagalink\book\front\page\status404\service\urlBuilder\FrontStatus404UrlBuilder;

final class
	FrontStatus404UrlTranslator
extends
	FrontUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FrontStatus404UrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\front\page\status404\library\controller\FrontStatus404Controller;
use tagalink\book\front\page\status404\library\segmentsMatcher\FrontStatus404SegmentsMatcher;
use tagalink\book\front\page\status404\library\urlTranslator\FrontStatus404UrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FrontStatus404Route
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FrontStatus404Controller();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FrontStatus404SegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FrontStatus404UrlTranslator();
	}

}
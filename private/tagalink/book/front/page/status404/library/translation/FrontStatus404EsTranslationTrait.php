<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\library\translation;

trait
	FrontStatus404EsTranslationTrait
{

	// --- pageFrontStatus404 ---

	public static function pageFrontStatus404_title():string { return 'Página No Encontrada'; }

	public static function pageFrontStatus404_segment():string { return 'pagina-no-encontrada'; }

	// --- widgetFrontStatus404Main ---

	public static function widgetFrontStatus404Main_h1():string { return self::pageFrontStatus404_title(); }

	public static function widgetFrontStatus404Main_message():string { return 'Ésta es la página '.self::pageFrontStatus404_title().'.'; }

}
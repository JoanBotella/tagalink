<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\library\translation;

interface
	FrontStatus404TranslationItf
{

	// --- pageFrontStatus404 ---

	public static function pageFrontStatus404_title():string;

	public static function pageFrontStatus404_segment():string;

	// --- widgetFrontStatus404Main ---

	public static function widgetFrontStatus404Main_h1():string;

	public static function widgetFrontStatus404Main_message():string;

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\library\translation;

trait
	FrontStatus404EnTranslationTrait
{

	// --- pageFrontStatus404 ---

	public static function pageFrontStatus404_title():string { return 'Page Not Found'; }

	public static function pageFrontStatus404_segment():string { return 'page-not-found'; }

	// --- widgetFrontStatus404Main ---

	public static function widgetFrontStatus404Main_h1():string { return self::pageFrontStatus404_title(); }

	public static function widgetFrontStatus404Main_message():string { return 'This is the '.self::pageFrontStatus404_title().' page'; }

}
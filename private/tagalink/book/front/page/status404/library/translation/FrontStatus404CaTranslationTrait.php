<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\library\translation;

trait
	FrontStatus404CaTranslationTrait
{

	// --- pageFrontStatus404 ---

	public static function pageFrontStatus404_title():string { return 'Pàgina No Trobada'; }

	public static function pageFrontStatus404_segment():string { return 'pagina-no-trobada'; }

	// --- widgetFrontStatus404Main ---

	public static function widgetFrontStatus404Main_h1():string { return self::pageFrontStatus404_title(); }

	public static function widgetFrontStatus404Main_message():string { return 'Ésta és la pàgina '.self::pageFrontStatus404_title().'.'; }

}
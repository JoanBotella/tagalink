<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\library\controller;

use tagalink\book\front\library\controller\FrontControllerAbs;
use tagalink\book\front\page\status404\service\responder\FrontStatus404Responder;

final class
	FrontStatus404Controller
extends
	FrontControllerAbs
{

	protected function respond():void
	{
		FrontStatus404Responder::run();
	}

}
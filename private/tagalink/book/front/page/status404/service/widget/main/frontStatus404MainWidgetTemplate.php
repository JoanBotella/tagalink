<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\page\status404\service\widget\main\FrontStatus404MainWidgetContext $context
 */

$translation = $context->getTranslation();

?>
		<main data-widget="front-status404-main">
			<h1><?= $translation->widgetFrontStatus404Main_h1() ?></h1>

			<p><?= $translation->widgetFrontStatus404Main_message() ?></p>
		</main>

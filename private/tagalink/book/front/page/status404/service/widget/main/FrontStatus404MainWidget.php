<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\service\widget\main;

final class
	FrontStatus404MainWidget
{

	public static function render(FrontStatus404MainWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontStatus404MainWidgetTemplate.php');
		return ob_get_clean();
	}

}
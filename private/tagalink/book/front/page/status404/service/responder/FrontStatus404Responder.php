<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status404\service\responder;

use tagalink\book\front\library\responder\FrontResponderAbs;
use tagalink\book\front\page\status404\service\urlBuilder\FrontStatus404UrlBuilder;
use tagalink\library\Constant;
use tagalink\book\front\page\status404\service\widget\main\FrontStatus404MainWidget;
use tagalink\book\front\page\status404\service\widget\main\FrontStatus404MainWidgetContext;

final class
	FrontStatus404Responder
extends
	FrontResponderAbs
{

	protected static function buildMainWidget():string
	{
		return FrontStatus404MainWidget::render(
			new FrontStatus404MainWidgetContext(
				static::getTranslation()
			)
		);
	}

	protected static function buildHttpStatus():int
	{
		return Constant::HTTP_STATUS_NOT_FOUND;
	}

	protected static function getDataPage():string
	{
		return 'status404';
	}

	protected static function buildCanonicalUrl():string
	{
		return FrontStatus404UrlBuilder::build();
	}

	protected static function getTitle():string
	{
		$translation = static::getTranslation();
		return $translation::pageFrontStatus404_title();
	}

}
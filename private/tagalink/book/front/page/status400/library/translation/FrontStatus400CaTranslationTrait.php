<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\translation;

trait
	FrontStatus400CaTranslationTrait
{

	// --- pageFrontStatus400 ---

	public static function pageFrontStatus400_title():string { return 'Error Intern del Servidor'; }

	public static function pageFrontStatus400_segment():string { return 'error-intern-del-servidor'; }

	// --- widgetFrontStatus400Main ---

	public static function widgetFrontStatus400Main_h1():string { return self::pageFrontStatus400_title(); }

	public static function widgetFrontStatus400Main_message():string { return 'Ésta és la pàgina '.self::pageFrontStatus400_title().'.'; }

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\translation;

trait
	FrontStatus400EnTranslationTrait
{

	// --- pageFrontStatus400 ---

	public static function pageFrontStatus400_title():string { return 'Internal Server Error'; }

	public static function pageFrontStatus400_segment():string { return 'internal-server-error'; }

	// --- widgetFrontStatus400Main ---

	public static function widgetFrontStatus400Main_h1():string { return self::pageFrontStatus400_title(); }

	public static function widgetFrontStatus400Main_message():string { return 'This is the '.self::pageFrontStatus400_title().' page'; }

}
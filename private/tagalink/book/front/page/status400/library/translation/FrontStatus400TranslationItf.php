<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\translation;

interface
	FrontStatus400TranslationItf
{

	// --- pageFrontStatus400 ---

	public static function pageFrontStatus400_title():string;

	public static function pageFrontStatus400_segment():string;

	// --- widgetFrontStatus400Main ---

	public static function widgetFrontStatus400Main_h1():string;

	public static function widgetFrontStatus400Main_message():string;

}
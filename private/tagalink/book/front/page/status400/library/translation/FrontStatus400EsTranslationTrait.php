<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\translation;

trait
	FrontStatus400EsTranslationTrait
{

	// --- pageFrontStatus400 ---

	public static function pageFrontStatus400_title():string { return 'Error Interno del Servidor'; }

	public static function pageFrontStatus400_segment():string { return 'error-interno-del-servidor'; }

	// --- widgetFrontStatus400Main ---

	public static function widgetFrontStatus400Main_h1():string { return self::pageFrontStatus400_title(); }

	public static function widgetFrontStatus400Main_message():string { return 'Ésta es la página '.self::pageFrontStatus400_title().'.'; }

}
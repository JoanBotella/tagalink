<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\urlTranslator;

use tagalink\book\front\library\urlTranslator\FrontUrlTranslatorAbs;
use tagalink\book\front\page\status400\service\urlBuilder\FrontStatus400UrlBuilder;

final class
	FrontStatus400UrlTranslator
extends
	FrontUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FrontStatus400UrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
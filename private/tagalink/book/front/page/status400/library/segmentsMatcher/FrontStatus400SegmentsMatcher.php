<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\segmentsMatcher;

use tagalink\book\front\library\segmentsMatcher\FrontSegmentsMatcherAbs;

final class
	FrontStatus400SegmentsMatcher
extends
	FrontSegmentsMatcherAbs
{

	public function match(array $segments):bool
	{
		if (count($segments) != 2)
		{
			return false;
		}

		$translation = $this->getTranslation();
		return $segments[1] == $translation::pageFrontStatus400_segment();
	}

}
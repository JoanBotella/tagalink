<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\controller;

use tagalink\book\front\library\controller\FrontControllerAbs;
use tagalink\book\front\page\status400\service\responder\FrontStatus400Responder;

final class
	FrontStatus400Controller
extends
	FrontControllerAbs
{

	protected function respond():void
	{
		FrontStatus400Responder::run();
	}

}
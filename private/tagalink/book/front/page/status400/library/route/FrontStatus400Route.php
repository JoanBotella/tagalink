<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\front\page\status400\library\controller\FrontStatus400Controller;
use tagalink\book\front\page\status400\library\segmentsMatcher\FrontStatus400SegmentsMatcher;
use tagalink\book\front\page\status400\library\urlTranslator\FrontStatus400UrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FrontStatus400Route
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FrontStatus400Controller();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FrontStatus400SegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FrontStatus400UrlTranslator();
	}

}
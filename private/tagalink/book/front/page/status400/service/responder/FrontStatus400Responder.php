<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\service\responder;

use tagalink\book\front\library\responder\FrontResponderAbs;
use tagalink\book\front\page\status400\service\urlBuilder\FrontStatus400UrlBuilder;
use tagalink\library\Constant;
use tagalink\book\front\page\status400\service\widget\main\FrontStatus400MainWidget;
use tagalink\book\front\page\status400\service\widget\main\FrontStatus400MainWidgetContext;

final class
	FrontStatus400Responder
extends
	FrontResponderAbs
{

	protected static function buildMainWidget():string
	{
		return FrontStatus400MainWidget::render(
			new FrontStatus400MainWidgetContext(
				static::getTranslation()
			)
		);
	}

	protected static function buildHttpStatus():int
	{
		return Constant::HTTP_STATUS_NOT_FOUND;
	}

	protected static function getDataPage():string
	{
		return 'status400';
	}

	protected static function buildCanonicalUrl():string
	{
		return FrontStatus400UrlBuilder::build();
	}

	protected static function getTitle():string
	{
		$translation = static::getTranslation();
		return $translation::pageFrontStatus400_title();
	}

}
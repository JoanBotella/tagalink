<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\service\urlBuilder;

use tagalink\book\front\library\urlBuilder\FrontUrlBuilderAbs;

final class
	FrontStatus400UrlBuilder
extends
	FrontUrlBuilderAbs
{

	public static function build():string
	{
		return self::buildByLanguageCode(
			static::getLanguageCode()
		);
	}

	public static function buildByLanguageCode(string $languageCode):string
	{
		$translation = static::getTranslationByLanguageCode($languageCode);
		return
			$languageCode
			.'/'.$translation::pageFrontStatus400_segment()
		;
	}

}
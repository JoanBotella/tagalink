<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\page\status400\service\widget\main\FrontStatus400MainWidgetContext $context
 */

$translation = $context->getTranslation();

?>
		<main data-widget="front-status400-main">
			<h1><?= $translation->widgetFrontStatus400Main_h1() ?></h1>

			<p><?= $translation->widgetFrontStatus400Main_message() ?></p>
		</main>

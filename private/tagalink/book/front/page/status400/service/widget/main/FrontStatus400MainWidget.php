<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\service\widget\main;

final class
	FrontStatus400MainWidget
{

	public static function render(FrontStatus400MainWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontStatus400MainWidgetTemplate.php');
		return ob_get_clean();
	}

}
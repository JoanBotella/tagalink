<?php
declare(strict_types=1);

namespace tagalink\book\front\page\status400\service\widget\main;

use tagalink\library\translation\TranslationItf;

final class
	FrontStatus400MainWidgetContext
{

	private TranslationItf $translation;

	public function __construct(
		TranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():TranslationItf
	{
		return $this->translation;
	}

}
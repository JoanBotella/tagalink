<?php
declare(strict_types=1);

namespace tagalink\book\front\library\requestValidator;

use tagalink\library\requestValidator\RequestValidatorAbs;

abstract class
	FrontRequestValidatorAbs
extends
	RequestValidatorAbs
{

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\library\translation;

use tagalink\book\front\page\home\library\translation\FrontHomeCaTranslationTrait;
use tagalink\book\front\page\login\library\translation\FrontLoginCaTranslationTrait;
use tagalink\book\front\page\register\library\translation\FrontRegisterCaTranslationTrait;
use tagalink\book\front\page\status400\library\translation\FrontStatus400CaTranslationTrait;
use tagalink\book\front\page\status404\library\translation\FrontStatus404CaTranslationTrait;

trait
	FrontCaTranslationTrait
{
	use
		FrontHomeCaTranslationTrait,
		FrontLoginCaTranslationTrait,
		FrontRegisterCaTranslationTrait,
		FrontStatus400CaTranslationTrait,
		FrontStatus404CaTranslationTrait
	;

	// --- widgetFrontFooter ---

	public static function widgetFrontFooter_createdBy():string { return 'Creado por:'; }

	// --- widgetFrontSystemLinks ---

	public static function widgetFrontSystemLinks_login():string { return 'Entrar'; }

	public static function widgetFrontSystemLinks_register():string { return 'Registre'; }

	public static function widgetFrontSystemLinks_logout():string { return 'Sortir'; }

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\library\translation;

use tagalink\book\front\page\home\library\translation\FrontHomeTranslationItf;
use tagalink\book\front\page\login\library\translation\FrontLoginTranslationItf;
use tagalink\book\front\page\register\library\translation\FrontRegisterTranslationItf;
use tagalink\book\front\page\status400\library\translation\FrontStatus400TranslationItf;
use tagalink\book\front\page\status404\library\translation\FrontStatus404TranslationItf;

interface
	FrontTranslationItf
extends
	FrontHomeTranslationItf,
	FrontLoginTranslationItf,
	FrontRegisterTranslationItf,
	FrontStatus400TranslationItf,
	FrontStatus404TranslationItf
{

	// --- widgetFrontFooter ---

	public static function widgetFrontFooter_createdBy():string;

	// --- widgetFrontSystemLinks ---

	public static function widgetFrontSystemLinks_login():string;

	public static function widgetFrontSystemLinks_register():string;

	public static function widgetFrontSystemLinks_logout():string;

}
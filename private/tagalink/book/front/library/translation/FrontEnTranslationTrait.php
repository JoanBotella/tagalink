<?php
declare(strict_types=1);

namespace tagalink\book\front\library\translation;

use tagalink\book\front\page\home\library\translation\FrontHomeEnTranslationTrait;
use tagalink\book\front\page\login\library\translation\FrontLoginEnTranslationTrait;
use tagalink\book\front\page\register\library\translation\FrontRegisterEnTranslationTrait;
use tagalink\book\front\page\status400\library\translation\FrontStatus400EnTranslationTrait;
use tagalink\book\front\page\status404\library\translation\FrontStatus404EnTranslationTrait;

trait
	FrontEnTranslationTrait
{
	use
		FrontHomeEnTranslationTrait,
		FrontLoginEnTranslationTrait,
		FrontRegisterEnTranslationTrait,
		FrontStatus400EnTranslationTrait,
		FrontStatus404EnTranslationTrait
	;

	// --- widgetFrontFooter ---

	public static function widgetFrontFooter_createdBy():string { return 'Created by:'; }

	// --- widgetFrontSystemLinks ---

	public static function widgetFrontSystemLinks_login():string { return 'Login'; }

	public static function widgetFrontSystemLinks_register():string { return 'Register'; }

	public static function widgetFrontSystemLinks_logout():string { return 'Logout'; }

}
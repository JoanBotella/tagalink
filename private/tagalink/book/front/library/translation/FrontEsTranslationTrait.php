<?php
declare(strict_types=1);

namespace tagalink\book\front\library\translation;

use tagalink\book\front\page\home\library\translation\FrontHomeEsTranslationTrait;
use tagalink\book\front\page\login\library\translation\FrontLoginEsTranslationTrait;
use tagalink\book\front\page\register\library\translation\FrontRegisterEsTranslationTrait;
use tagalink\book\front\page\status400\library\translation\FrontStatus400EsTranslationTrait;
use tagalink\book\front\page\status404\library\translation\FrontStatus404EsTranslationTrait;

trait
	FrontEsTranslationTrait
{
	use
		FrontHomeEsTranslationTrait,
		FrontLoginEsTranslationTrait,
		FrontRegisterEsTranslationTrait,
		FrontStatus400EsTranslationTrait,
		FrontStatus404EsTranslationTrait
	;

	// --- widgetFrontFooter ---

	public static function widgetFrontFooter_createdBy():string { return 'Creado por:'; }

	// --- widgetFrontSystemLinks ---

	public static function widgetFrontSystemLinks_login():string { return 'Entrar'; }

	public static function widgetFrontSystemLinks_register():string { return 'Registro'; }

	public static function widgetFrontSystemLinks_logout():string { return 'Salir'; }

}
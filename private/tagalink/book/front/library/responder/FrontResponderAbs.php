<?php
declare(strict_types=1);

namespace tagalink\book\front\library\responder;

use tagalink\book\form\page\logout\service\urlBuilder\FormLogoutUrlBuilder;
use tagalink\library\responder\ResponderAbs;
use tagalink\book\front\page\home\service\urlBuilder\FrontHomeUrlBuilder;
use tagalink\book\front\page\login\service\urlBuilder\FrontLoginUrlBuilder;
use tagalink\book\front\page\register\service\urlBuilder\FrontRegisterUrlBuilder;
use tagalink\service\configuration\Configuration;
use tagalink\service\languageCodeContainer\LanguageCodeContainer;
use tagalink\service\pathContainer\PathContainer;
use tagalink\service\translationContainer\TranslationContainer;
use tagalink\book\front\service\widget\body\FrontBodyWidget;
use tagalink\book\front\service\widget\body\FrontBodyWidgetContext;
use tagalink\book\front\service\widget\footer\FrontFooterWidget;
use tagalink\book\front\service\widget\footer\FrontFooterWidgetContext;
use tagalink\book\front\service\widget\header\FrontHeaderWidget;
use tagalink\book\front\service\widget\header\FrontHeaderWidgetContext;
use tagalink\service\widget\html\HtmlWidget;
use tagalink\service\widget\html\HtmlWidgetContext;
use tagalink\book\front\service\widget\nav\FrontNavWidget;
use tagalink\book\front\service\widget\nav\FrontNavWidgetContext;
use tagalink\book\front\service\widget\notifications\FrontNotificationsWidget;
use tagalink\book\front\service\widget\notifications\FrontNotificationsWidgetContext;
use tagalink\book\front\service\widget\systemLinks\FrontSystemLinksWidget;
use tagalink\book\front\service\widget\systemLinks\FrontSystemLinksWidgetContext;
use tagalink\library\translation\TranslationItf;
use tagalink\service\userContainer\UserContainer;
use tagalink\service\notificationContainer\NotificationContainer;

abstract class
	FrontResponderAbs
extends
	ResponderAbs
{

	protected static function buildBody():string
	{
		$frontHeaderWidget = FrontHeaderWidget::render(
			new FrontHeaderWidgetContext(
				FrontHomeUrlBuilder::build(),
				static::buildFrontNavWidget()
			)
		);

		$mainWidget = static::buildMainWidget();

		$frontFooterWidget = FrontFooterWidget::render(
			new FrontFooterWidgetContext(
				static::getTranslation()
			)
		);

		$frontBodyWidget = FrontBodyWidget::render(
			new FrontBodyWidgetContext(
				static::getDataPage(),
				static::buildFrontNotificationsWidget(),
				$frontHeaderWidget,
				$mainWidget,
				$frontFooterWidget
			)
		);

		$htmlWidgetContext = new HtmlWidgetContext(
			LanguageCodeContainer::get(),
			Configuration::getPathBase(),
			self::buildTitle(),
			$frontBodyWidget
		);

		$canonicalUrl = static::buildCanonicalUrl();

		if ($canonicalUrl != PathContainer::getPath())
		{
			$htmlWidgetContext->setCanonicalUrl($canonicalUrl);
		}

		return HtmlWidget::render($htmlWidgetContext);
	}

		private static function buildFrontNotificationsWidget():string
		{
			$frontNotificationsWidgetContext = new FrontNotificationsWidgetContext();

			$frontNotificationsWidgetContext->setErrors(
				NotificationContainer::getErrorsAndUnset()
			);

			$frontNotificationsWidgetContext->setWarnings(
				NotificationContainer::getWarningsAndUnset()
			);

			$frontNotificationsWidgetContext->setSuccesses(
				NotificationContainer::getSuccessesAndUnset()
			);

			$frontNotificationsWidgetContext->setInfos(
				NotificationContainer::getInfosAndUnset()
			);

			return FrontNotificationsWidget::render(
				$frontNotificationsWidgetContext
			);
		}

		private static function buildFrontNavWidget():string
		{
			$navWidgetContext = new FrontNavWidgetContext(
				static::buildFrontSystemLinksWidget()
			);

			return FrontNavWidget::render(
				$navWidgetContext
			);
	}

			private static function buildFrontSystemLinksWidget():string
			{
				$frontSystemLinksWidgetContext = new FrontSystemLinksWidgetContext(
					static::getTranslation()
				);

				if (UserContainer::has())
				{
					$frontSystemLinksWidgetContext->setLogoutUrl(
						FormLogoutUrlBuilder::build()
					);
				}
				else
				{
					$frontSystemLinksWidgetContext->setLoginUrl(
						FrontLoginUrlBuilder::build()
					);

					$frontSystemLinksWidgetContext->setRegisterUrl(
						FrontRegisterUrlBuilder::build()
					);
				}

				return FrontSystemLinksWidget::render(
					$frontSystemLinksWidgetContext
				);
			}

		protected static function getTranslation():TranslationItf
		{
			return TranslationContainer::get();
		}

		abstract protected static function buildMainWidget():string;

		abstract protected static function getDataPage():string;

		abstract protected static function buildCanonicalUrl():string;

		protected static function buildTitle():string
		{
			return implode(
				' | ',
				[
					static::getTitle(),
					'Tagalink',
				]
			);
		}

			abstract protected static function getTitle():string;

}
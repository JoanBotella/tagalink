<?php
declare(strict_types=1);

namespace tagalink\book\front\library\segmentsMatcher;

use tagalink\library\segmentsMatcher\SegmentsMatcherAbs;

abstract class
	FrontSegmentsMatcherAbs
extends
	SegmentsMatcherAbs
{

}
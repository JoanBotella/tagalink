<?php
declare(strict_types=1);

namespace tagalink\book\front\library\urlTranslator;

use tagalink\library\urlTranslator\UrlTranslatorAbs;

abstract class
	FrontUrlTranslatorAbs
extends
	UrlTranslatorAbs
{

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\library\urlBuilder;

use tagalink\library\urlBuilder\UrlBuilderAbs;

abstract class
	FrontUrlBuilderAbs
extends
	UrlBuilderAbs
{

}
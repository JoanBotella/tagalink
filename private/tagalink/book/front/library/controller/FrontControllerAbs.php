<?php
declare(strict_types=1);

namespace tagalink\book\front\library\controller;

use tagalink\library\controller\ControllerAbs;

abstract class
	FrontControllerAbs
extends
	ControllerAbs
{

}
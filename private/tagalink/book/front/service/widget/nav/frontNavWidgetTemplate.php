<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\service\widget\nav\FrontNavWidgetContext $context
 */

?>
		<nav data-widget="front-nav">
			<ul>
			</ul>

<?= $context->getFrontSystemLinksWidget() ?>

		</nav>

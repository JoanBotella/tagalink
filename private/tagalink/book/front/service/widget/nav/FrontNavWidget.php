<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\nav;

final class
	FrontNavWidget
{

	public static function render(FrontNavWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontNavWidgetTemplate.php');
		return ob_get_clean();
	}

}
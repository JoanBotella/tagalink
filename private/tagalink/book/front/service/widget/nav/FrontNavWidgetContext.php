<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\nav;

final class
	FrontNavWidgetContext
{

	private string $frontSystemLinksWidget;

	public function __construct(
		string $frontSystemLinksWidget
	)
	{
		$this->frontSystemLinksWidget = $frontSystemLinksWidget;
	}

	public function getFrontSystemLinksWidget():string
	{
		return $this->frontSystemLinksWidget;
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\systemLinks;

final class
	FrontSystemLinksWidget
{

	public static function render(FrontSystemLinksWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontSystemLinksWidgetTemplate.php');
		return ob_get_clean();
	}

}
<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\service\widget\systemLinks\FrontSystemLinksWidgetContext $context
 */

$translation = $context->getTranslation();

?>
			<ul data-widget="front-system_links">
<?php if ($context->hasLoginUrl()): ?>

				<li>
					<a href="<?= $context->getLoginUrlAfterHas() ?>"><?= $translation->widgetFrontSystemLinks_login() ?></a>
				</li>
<?php endif ?>

<?php if ($context->hasRegisterUrl()): ?>

				<li>
					<a href="<?= $context->getRegisterUrlAfterHas() ?>"><?= $translation->widgetFrontSystemLinks_register() ?></a>
				</li>
<?php endif ?>

<?php if ($context->hasLogoutUrl()): ?>

				<li>
					<a href="<?= $context->getLogoutUrlAfterHas() ?>"><?= $translation->widgetFrontSystemLinks_logout() ?></a>
				</li>
<?php endif ?>

			</ul>
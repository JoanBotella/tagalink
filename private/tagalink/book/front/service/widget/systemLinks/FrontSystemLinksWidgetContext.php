<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\systemLinks;

use tagalink\library\translation\TranslationItf;

final class
	FrontSystemLinksWidgetContext
{

	private TranslationItf $translation;
	private string $loginUrl;
	private string $registerUrl;
	private string $logoutUrl;

	public function __construct(
		TranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():TranslationItf
	{
		return $this->translation;
	}

	public function hasLoginUrl():bool
	{
		return isset($this->loginUrl);
	}

	public function getLoginUrlAfterHas():string
	{
		return $this->loginUrl;
	}

	public function setLoginUrl(string $v):void
	{
		$this->loginUrl = $v;
	}

	public function unsetLoginUrl():void
	{
		unset($this->loginUrl);
	}

	public function hasRegisterUrl():bool
	{
		return isset($this->registerUrl);
	}

	public function getRegisterUrlAfterHas():string
	{
		return $this->registerUrl;
	}

	public function setRegisterUrl(string $v):void
	{
		$this->registerUrl = $v;
	}

	public function unsetRegisterUrl():void
	{
		unset($this->registerUrl);
	}

	public function hasLogoutUrl():bool
	{
		return isset($this->logoutUrl);
	}

	public function getLogoutUrlAfterHas():string
	{
		return $this->logoutUrl;
	}

	public function setLogoutUrl(string $v):void
	{
		$this->logoutUrl = $v;
	}

	public function unsetLogoutUrl():void
	{
		unset($this->logoutUrl);
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\notifications;

final class
	FrontNotificationsWidget
{

	public static function render(FrontNotificationsWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontNotificationsWidgetTemplate.php');
		return ob_get_clean();
	}

}
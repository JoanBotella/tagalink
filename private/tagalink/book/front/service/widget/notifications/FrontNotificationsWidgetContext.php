<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\notifications;

final class
	FrontNotificationsWidgetContext
{

	private array $errors;

	public function hasErrors():bool
	{
		return isset($this->errors);
	}

	public function getErrorsAfterHas():array
	{
		return $this->errors;
	}

	public function setErrors(array $v):void
	{
		$this->errors = $v;
	}

	public function unsetErrors():void
	{
		unset($this->errors);
	}

	private array $warnings;

	public function hasWarnings():bool
	{
		return isset($this->warnings);
	}

	public function getWarningsAfterHas():array
	{
		return $this->warnings;
	}

	public function setWarnings(array $v):void
	{
		$this->warnings = $v;
	}

	public function unsetWarnings():void
	{
		unset($this->warnings);
	}

	private array $successes;

	public function hasSuccesses():bool
	{
		return isset($this->successes);
	}

	public function getSuccessesAfterHas():array
	{
		return $this->successes;
	}

	public function setSuccesses(array $v):void
	{
		$this->successes = $v;
	}

	public function unsetSuccesses():void
	{
		unset($this->successes);
	}

	private array $infos;

	public function hasInfos():bool
	{
		return isset($this->infos);
	}

	public function getInfosAfterHas():array
	{
		return $this->infos;
	}

	public function setInfos(array $v):void
	{
		$this->infos = $v;
	}

	public function unsetInfos():void
	{
		unset($this->infos);
	}

}
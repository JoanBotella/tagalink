<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\service\widget\notifications\FrontNotificationsWidgetContext $context
 */

?>
	<div data-widget="front-notifications">
<?php if ($context->hasErrors()): ?>

		<ul class="widget-front-notifications__errors">
<?php foreach ($context->getErrorsAfterHas() as $error): ?>

			<li><?= $error ?></li>
<?php endforeach ?>

		</ul>
<?php endif ?>

<?php if ($context->hasWarnings()): ?>

		<ul class="widget-front-notifications__warnings">
<?php foreach ($context->getWarningsAfterHas() as $warning): ?>

			<li><?= $warning ?></li>
<?php endforeach ?>

		</ul>
<?php endif ?>

<?php if ($context->hasSuccesses()): ?>

		<ul class="widget-front-notifications__successes">
<?php foreach ($context->getSuccessesAfterHas() as $success): ?>

			<li><?= $success ?></li>
<?php endforeach ?>

		</ul>
<?php endif ?>

<?php if ($context->hasInfos()): ?>

		<ul class="widget-front-notifications__infos">
<?php foreach ($context->getInfosAfterHas() as $info): ?>

			<li><?= $info ?></li>
<?php endforeach ?>

		</ul>
<?php endif ?>

	</div>
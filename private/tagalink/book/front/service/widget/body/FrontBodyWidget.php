<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\body;

final class
	FrontBodyWidget
{

	public static function render(FrontBodyWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontBodyWidgetTemplate.php');
		return ob_get_clean();
	}

}
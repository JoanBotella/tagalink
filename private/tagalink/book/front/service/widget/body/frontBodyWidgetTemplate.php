<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\service\widget\body\FrontBodyWidgetContext $context
 */

?>
<body data-book="front" data-page="<?= $context->getDataPage() ?>" data-widget="front-body">
<?= $context->getFrontNotificationsWidget() ?>
<?= $context->getFrontHeaderWidget() ?>
	<div class="widget-front-body__main_container">
<?= $context->getMainWidget() ?>
	</div>
<?= $context->getFrontFooterWidget() ?>
</body>

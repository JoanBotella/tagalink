<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\body;

final class
	FrontBodyWidgetContext
{

	private string $dataPage;
	private string $frontNotificationsWidget;
	private string $frontHeaderWidget;
	private string $mainWidget;
	private string $frontFooterWidget;

	public function __construct(
		string $dataPage,
		string $frontNotificationsWidget,
		string $frontHeaderWidget,
		string $mainWidget,
		string $frontFooterWidget
	)
	{
		$this->dataPage = $dataPage;
		$this->frontNotificationsWidget = $frontNotificationsWidget;
		$this->frontHeaderWidget = $frontHeaderWidget;
		$this->mainWidget = $mainWidget;
		$this->frontFooterWidget = $frontFooterWidget;
	}

	public function getDataPage():string
	{
		return $this->dataPage;
	}

	public function getFrontNotificationsWidget():string
	{
		return $this->frontNotificationsWidget;
	}

	public function getFrontHeaderWidget():string
	{
		return $this->frontHeaderWidget;
	}

	public function getMainWidget():string
	{
		return $this->mainWidget;
	}

	public function getFrontFooterWidget():string
	{
		return $this->frontFooterWidget;
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\header;

final class
	FrontHeaderWidgetContext
{

	private string $logoLinkUrl;
	private string $navWidget;

	public function __construct(
		string $logoLinkUrl,
		string $navWidget
	)
	{
		$this->logoLinkUrl = $logoLinkUrl;
		$this->navWidget = $navWidget;
	}

	public function getLogoLinkUrl():string
	{
		return $this->logoLinkUrl;
	}

	public function getFrontNavWidget():string
	{
		return $this->navWidget;
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\header;

final class
	FrontHeaderWidget
{

	public static function render(FrontHeaderWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontHeaderWidgetTemplate.php');
		return ob_get_clean();
	}

}
<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\service\widget\header\FrontHeaderWidgetContext $context
 */

?>
	<header data-widget="front-header">
		<a class="widget-front-header__logo_link" href="<?= $context->getLogoLinkUrl() ?>">Tagalink</a>
<?= $context->getFrontNavWidget() ?>
	</header>

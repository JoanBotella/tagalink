<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\footer;

use tagalink\library\translation\TranslationItf;

final class
	FrontFooterWidgetContext
{

	private TranslationItf $translation;

	public function __construct(
		TranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():TranslationItf
	{
		return $this->translation;
	}

}
<?php
declare(strict_types=1);

/**
 * @var tagalink\book\front\service\widget\footer\FrontFooterWidgetContext $context
 */

$translation = $context->getTranslation();

?>
	<footer data-widget="front-footer">
		<nav>
			<ul>
				<li>
					<?= $translation->widgetFrontFooter_createdBy() ?> <a href="https://joanbotella.com" target="_blank">joanbotella.com</a>
				</li>
			</ul>
		</nav>
	</footer>

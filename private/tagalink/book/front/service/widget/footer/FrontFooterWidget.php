<?php
declare(strict_types=1);

namespace tagalink\book\front\service\widget\footer;

final class
	FrontFooterWidget
{

	public static function render(FrontFooterWidgetContext $context):string
	{
		ob_start();
		require(__DIR__.'/frontFooterWidgetTemplate.php');
		return ob_get_clean();
	}

}
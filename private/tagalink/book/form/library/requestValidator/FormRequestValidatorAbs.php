<?php
declare(strict_types=1);

namespace tagalink\book\form\library\requestValidator;

use tagalink\library\requestValidator\RequestValidatorAbs;

abstract class
	FormRequestValidatorAbs
extends
	RequestValidatorAbs
{

}
<?php
declare(strict_types=1);

namespace tagalink\book\form\library\translation;

use tagalink\book\form\page\login\library\translation\FormLoginEsTranslationTrait;
use tagalink\book\form\page\logout\library\translation\FormLogoutEsTranslationTrait;
use tagalink\book\form\page\register\library\translation\FormRegisterEsTranslationTrait;

trait
	FormEsTranslationTrait
{
	use
		FormLoginEsTranslationTrait,
		FormLogoutEsTranslationTrait,
		FormRegisterEsTranslationTrait
	;

	// --- bookForm ---

	public static function bookForm_segment():string { return 'form'; }

}
<?php
declare(strict_types=1);

namespace tagalink\book\form\library\translation;

use tagalink\book\form\page\login\library\translation\FormLoginEnTranslationTrait;
use tagalink\book\form\page\logout\library\translation\FormLogoutEnTranslationTrait;
use tagalink\book\form\page\register\library\translation\FormRegisterEnTranslationTrait;

trait
	FormEnTranslationTrait
{
	use
		FormLoginEnTranslationTrait,
		FormLogoutEnTranslationTrait,
		FormRegisterEnTranslationTrait
	;

	// --- bookForm ---

	public static function bookForm_segment():string { return 'form'; }

}
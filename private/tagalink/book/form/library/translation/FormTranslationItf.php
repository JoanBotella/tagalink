<?php
declare(strict_types=1);

namespace tagalink\book\form\library\translation;

use tagalink\book\form\page\login\library\translation\FormLoginTranslationItf;
use tagalink\book\form\page\logout\library\translation\FormLogoutTranslationItf;
use tagalink\book\form\page\register\library\translation\FormRegisterTranslationItf;

interface
	FormTranslationItf
extends
	FormLoginTranslationItf,
	FormLogoutTranslationItf,
	FormRegisterTranslationItf
{

	// --- bookForm ---

	public static function bookForm_segment():string;

}
<?php
declare(strict_types=1);

namespace tagalink\book\form\library\translation;

use tagalink\book\form\page\login\library\translation\FormLoginCaTranslationTrait;
use tagalink\book\form\page\logout\library\translation\FormLogoutCaTranslationTrait;
use tagalink\book\form\page\register\library\translation\FormRegisterCaTranslationTrait;

trait
	FormCaTranslationTrait
{
	use
		FormLoginCaTranslationTrait,
		FormLogoutCaTranslationTrait,
		FormRegisterCaTranslationTrait
	;

	// --- bookForm ---

	public static function bookForm_segment():string { return 'form'; }

}
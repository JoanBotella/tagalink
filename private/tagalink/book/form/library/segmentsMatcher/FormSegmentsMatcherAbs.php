<?php
declare(strict_types=1);

namespace tagalink\book\form\library\segmentsMatcher;

use tagalink\library\segmentsMatcher\SegmentsMatcherAbs;

abstract class
	FormSegmentsMatcherAbs
extends
	SegmentsMatcherAbs
{

	public function match(array $segments):bool
	{
		if (count($segments) != 3)
		{
			return false;
		}

		$translation = $this->getTranslation();
		return $segments[1] == $translation::bookForm_segment();
	}

}
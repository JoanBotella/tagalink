<?php
declare(strict_types=1);

namespace tagalink\book\form\library\urlBuilder;

use tagalink\library\urlBuilder\UrlBuilderAbs;

abstract class
	FormUrlBuilderAbs
extends
	UrlBuilderAbs
{

	public static function buildByLanguageCode(string $languageCode):string
	{
		$translation = static::getTranslationByLanguageCode($languageCode);
		return
			$languageCode
			.'/'.$translation::bookForm_segment()
		;
	}

}
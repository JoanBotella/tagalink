<?php
declare(strict_types=1);

namespace tagalink\book\form\library\responder;

use tagalink\library\responder\ResponderAbs;
use tagalink\service\absoluteUrlBuilder\AbsoluteUrlBuilder;

abstract class
	FormResponderAbs
extends
	ResponderAbs
{

	protected static function buildHeaders():array
	{
		$headers = parent::buildHeaders();
		$headers[] = 'Location: '.static::buildLocationHeaderAbsoluteUrl();
		return $headers;
	}

		private static function buildLocationHeaderAbsoluteUrl():string
		{
			return AbsoluteUrlBuilder::build(
				static::buildLocationHeaderRelativeUrl()
			);
		}

			abstract protected static function buildLocationHeaderRelativeUrl():string;

}
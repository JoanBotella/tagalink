<?php
declare(strict_types=1);

namespace tagalink\book\form\library\urlTranslator;

use tagalink\library\urlTranslator\UrlTranslatorAbs;

abstract class
	FormUrlTranslatorAbs
extends
	UrlTranslatorAbs
{

}
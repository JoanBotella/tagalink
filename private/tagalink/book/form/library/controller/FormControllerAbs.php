<?php
declare(strict_types=1);

namespace tagalink\book\form\library\controller;

use tagalink\library\controller\ControllerAbs;

abstract class
	FormControllerAbs
extends
	ControllerAbs
{

}
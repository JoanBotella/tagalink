<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\translation;

trait
	FormLogoutEsTranslationTrait
{

	// --- pageFormLogout ---

	public static function pageFormLogout_segment():string { return 'salir'; }

	public static function pageFormLogout_success():string { return 'Has salido exitosamente.'; }

}
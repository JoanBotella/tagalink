<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\translation;

trait
	FormLogoutCaTranslationTrait
{

	// --- pageFormLogout ---

	public static function pageFormLogout_segment():string { return 'sortir'; }

	public static function pageFormLogout_success():string { return 'Has sortit exitosament.'; }

}
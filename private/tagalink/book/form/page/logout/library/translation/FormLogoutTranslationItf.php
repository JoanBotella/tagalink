<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\translation;

interface
	FormLogoutTranslationItf
{

	// --- pageFormLogout ---

	public static function pageFormLogout_segment():string;

	public static function pageFormLogout_success():string;

}
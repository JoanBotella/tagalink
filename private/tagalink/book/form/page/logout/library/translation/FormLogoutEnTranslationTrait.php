<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\translation;

trait
	FormLogoutEnTranslationTrait
{

	// --- pageFormLogout ---

	public static function pageFormLogout_segment():string { return 'logout'; }

	public static function pageFormLogout_success():string { return 'You logged out successfully.'; }

}
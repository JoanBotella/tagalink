<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\segmentsMatcher;

use tagalink\book\form\library\segmentsMatcher\FormSegmentsMatcherAbs;

final class
	FormLogoutSegmentsMatcher
extends
	FormSegmentsMatcherAbs
{

	public function match(array $segments):bool
	{
		if (!parent::match($segments))
		{
			return false;
		}
		$translation = $this->getTranslation();
		return $segments[2] == $translation::pageFormLogout_segment();
	}

}
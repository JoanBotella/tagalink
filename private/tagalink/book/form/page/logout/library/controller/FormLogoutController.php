<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\controller;

use tagalink\book\form\library\controller\FormControllerAbs;
use tagalink\book\form\page\logout\service\responder\FormLogoutResponder;
use tagalink\service\notificationContainer\NotificationContainer;
use tagalink\service\translationContainer\TranslationContainer;
use tagalink\service\useCase\logout\LogoutUseCase;

final class
	FormLogoutController
extends
	FormControllerAbs
{

	protected function runUseCase():void
	{
		LogoutUseCase::run();

		if (LogoutUseCase::hasErrorCodes())
		{
			return;
		}

		$translation = TranslationContainer::get();

		NotificationContainer::addSuccess(
			$translation->pageFormLogout_success()
		);
	}

	protected function respond():void
	{
		FormLogoutResponder::run();
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\urlTranslator;

use tagalink\book\form\library\urlTranslator\FormUrlTranslatorAbs;
use tagalink\book\form\page\logout\service\urlBuilder\FormLogoutUrlBuilder;

final class
	FormLogoutUrlTranslator
extends
	FormUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FormLogoutUrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\form\page\logout\library\controller\FormLogoutController;
use tagalink\book\form\page\logout\library\segmentsMatcher\FormLogoutSegmentsMatcher;
use tagalink\book\form\page\logout\library\urlTranslator\FormLogoutUrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FormLogoutRoute
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FormLogoutController();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FormLogoutSegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FormLogoutUrlTranslator();
	}

}
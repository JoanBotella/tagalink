<?php
declare(strict_types=1);

namespace tagalink\book\form\page\logout\service\urlBuilder;

use tagalink\book\form\library\urlBuilder\FormUrlBuilderAbs;

final class
	FormLogoutUrlBuilder
extends
	FormUrlBuilderAbs
{

	public static function build():string
	{
		return self::buildByLanguageCode(
			static::getLanguageCode()
		);
	}

	public static function buildByLanguageCode(string $languageCode):string
	{
		$translation = static::getTranslationByLanguageCode($languageCode);
		return
			parent::buildByLanguageCode($languageCode)
			.'/'.$translation::pageFormLogout_segment()
		;
	}

}
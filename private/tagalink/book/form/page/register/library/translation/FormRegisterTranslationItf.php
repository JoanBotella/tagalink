<?php
declare(strict_types=1);

namespace tagalink\book\form\page\register\library\translation;

interface
	FormRegisterTranslationItf
{

	// --- pageFormRegister ---

	public static function pageFormRegister_segment():string;

}
<?php
declare(strict_types=1);

namespace tagalink\book\form\page\register\library\translation;

trait
	FormRegisterCaTranslationTrait
{

	// --- pageFormRegister ---

	public static function pageFormRegister_segment():string { return 'registre'; }

}
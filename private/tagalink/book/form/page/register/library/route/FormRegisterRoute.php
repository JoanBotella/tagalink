<?php
declare(strict_types=1);

namespace tagalink\book\form\page\register\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\form\page\register\library\controller\FormRegisterController;
use tagalink\book\form\page\register\library\segmentsMatcher\FormRegisterSegmentsMatcher;
use tagalink\book\form\page\register\library\urlTranslator\FormRegisterUrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FormRegisterRoute
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FormRegisterController();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FormRegisterSegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FormRegisterUrlTranslator();
	}

}
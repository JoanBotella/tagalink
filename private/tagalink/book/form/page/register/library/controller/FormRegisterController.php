<?php
declare(strict_types=1);

namespace tagalink\book\form\page\register\library\controller;

use tagalink\book\form\library\controller\FormControllerAbs;
use tagalink\book\form\page\register\service\responder\FormRegisterResponder;
use tagalink\library\Constant;
use tagalink\service\useCase\register\RegisterUseCase;

final class
	FormRegisterController
extends
	FormControllerAbs
{

	protected function isRequestValid():bool
	{
		return
			isset($_POST[Constant::FORM_REGISTER_EMAIL_NAME])
			&& isset($_POST[Constant::FORM_REGISTER_PASSWORD_NAME])
			&& isset($_POST[Constant::FORM_REGISTER_USERNAME_NAME])
		;
	}

	protected function runUseCase():void
	{
		RegisterUseCase::run(
			$_POST[Constant::FORM_REGISTER_EMAIL_NAME],
			$_POST[Constant::FORM_REGISTER_PASSWORD_NAME],
			$_POST[Constant::FORM_REGISTER_USERNAME_NAME]
		);
	}

	protected function respond():void
	{
		FormRegisterResponder::run();
	}

}
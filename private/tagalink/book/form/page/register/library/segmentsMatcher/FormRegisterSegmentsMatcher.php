<?php
declare(strict_types=1);

namespace tagalink\book\form\page\register\library\segmentsMatcher;

use tagalink\book\form\library\segmentsMatcher\FormSegmentsMatcherAbs;

final class
	FormRegisterSegmentsMatcher
extends
	FormSegmentsMatcherAbs
{

	public function match(array $segments):bool
	{
		if (!parent::match($segments))
		{
			return false;
		}
		$translation = $this->getTranslation();
		return $segments[2] == $translation::pageFormRegister_segment();
	}

}
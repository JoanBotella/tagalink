<?php
declare(strict_types=1);

namespace tagalink\book\form\page\register\library\urlTranslator;

use tagalink\book\form\library\urlTranslator\FormUrlTranslatorAbs;
use tagalink\book\form\page\register\service\urlBuilder\FormRegisterUrlBuilder;

final class
	FormRegisterUrlTranslator
extends
	FormUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FormRegisterUrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
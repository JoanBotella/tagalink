<?php
declare(strict_types=1);

namespace tagalink\book\form\page\register\service\responder;

use tagalink\book\form\library\responder\FormResponderAbs;
use tagalink\book\front\page\home\service\urlBuilder\FrontHomeUrlBuilder;
use tagalink\library\Constant;

final class
	FormRegisterResponder
extends
	FormResponderAbs
{

	protected static function buildHttpStatus():int
	{
		return Constant::HTTP_STATUS_SEE_OTHER;
	}

	protected static function buildLocationHeaderRelativeUrl():string
	{
		return FrontHomeUrlBuilder::build();
	}

}
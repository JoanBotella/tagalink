<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\service\urlBuilder;

use tagalink\book\form\library\urlBuilder\FormUrlBuilderAbs;

final class
	FormLoginUrlBuilder
extends
	FormUrlBuilderAbs
{

	public static function build():string
	{
		return self::buildByLanguageCode(
			static::getLanguageCode()
		);
	}

	public static function buildByLanguageCode(string $languageCode):string
	{
		$translation = static::getTranslationByLanguageCode($languageCode);
		return
			parent::buildByLanguageCode($languageCode)
			.'/'.$translation::pageFormLogin_segment()
		;
	}

}
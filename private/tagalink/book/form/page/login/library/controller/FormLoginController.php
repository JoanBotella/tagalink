<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\library\controller;

use tagalink\book\form\library\controller\FormControllerAbs;
use tagalink\book\form\page\login\service\responder\FormLoginResponder;
use tagalink\library\Constant;
use tagalink\service\notificationContainer\NotificationContainer;
use tagalink\service\translationContainer\TranslationContainer;
use tagalink\service\useCase\login\LoginUseCase;

final class
	FormLoginController
extends
	FormControllerAbs
{

	protected function isRequestValid():bool
	{
		return
			isset($_POST[Constant::FORM_LOGIN_EMAIL_NAME])
			&& isset($_POST[Constant::FORM_LOGIN_PASSWORD_NAME])
		;
	}

	protected function runUseCase():void
	{
		LoginUseCase::run(
			$_POST[Constant::FORM_LOGIN_EMAIL_NAME],
			$_POST[Constant::FORM_LOGIN_PASSWORD_NAME]
		);

		$translation = TranslationContainer::get();

		if (LoginUseCase::hasErrorCodes())
		{
			$errorCodes = LoginUseCase::getErrorCodesAfterHas();

			if (in_array(LoginUseCase::ERROR_CODE_USER_NOT_FOUND, $errorCodes))
			{
				NotificationContainer::addError(
					$translation->pageFormLogin_errorUserNotFound()
				);
			}
			return;
		}

		NotificationContainer::addSuccess(
			$translation->pageFormLogin_success()
		);
	}

	protected function respond():void
	{
		FormLoginResponder::run();
	}

}
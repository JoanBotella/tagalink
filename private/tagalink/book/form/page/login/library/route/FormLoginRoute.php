<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\library\route;

use tagalink\library\route\RouteAbs;
use tagalink\book\form\page\login\library\controller\FormLoginController;
use tagalink\book\form\page\login\library\segmentsMatcher\FormLoginSegmentsMatcher;
use tagalink\book\form\page\login\library\urlTranslator\FormLoginUrlTranslator;
use tagalink\library\controller\ControllerItf;
use tagalink\library\segmentsMatcher\SegmentsMatcherItf;
use tagalink\library\urlTranslator\UrlTranslatorItf;

final class
	FormLoginRoute
extends
	RouteAbs
{

	public function buildController():ControllerItf
	{
		return new FormLoginController();
	}

	public function buildSegmentsMatcher():SegmentsMatcherItf
	{
		return new FormLoginSegmentsMatcher();
	}

	public function buildUrlTranslator():UrlTranslatorItf
	{
		return new FormLoginUrlTranslator();
	}

}
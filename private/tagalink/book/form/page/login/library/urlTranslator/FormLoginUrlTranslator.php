<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\library\urlTranslator;

use tagalink\book\form\library\urlTranslator\FormUrlTranslatorAbs;
use tagalink\book\form\page\login\service\urlBuilder\FormLoginUrlBuilder;

final class
	FormLoginUrlTranslator
extends
	FormUrlTranslatorAbs
{

	public function translateBySegments(string $languageCode, array $segments):string
	{
		return FormLoginUrlBuilder::buildByLanguageCode(
			$languageCode
		);
	}

}
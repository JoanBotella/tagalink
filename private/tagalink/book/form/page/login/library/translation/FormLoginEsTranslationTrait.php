<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\library\translation;

trait
	FormLoginEsTranslationTrait
{

	// --- pageFormLogin ---

	public static function pageFormLogin_segment():string { return 'entrar'; }

	public static function pageFormLogin_errorUserNotFound():string { return 'No existe ningún usuario con esa dirección de e-mail y contraseña.'; }

	public static function pageFormLogin_success():string { return 'Has entrado exitosamente.'; }

}
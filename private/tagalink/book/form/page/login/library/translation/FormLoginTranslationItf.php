<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\library\translation;

interface
	FormLoginTranslationItf
{

	// --- pageFormLogin ---

	public static function pageFormLogin_segment():string;

	public static function pageFormLogin_errorUserNotFound():string;

	public static function pageFormLogin_success():string;

}
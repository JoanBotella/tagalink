<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\library\translation;

trait
	FormLoginEnTranslationTrait
{

	// --- pageFormLogin ---

	public static function pageFormLogin_segment():string { return 'login'; }

	public static function pageFormLogin_errorUserNotFound():string { return 'No user exists with that e-mail address and password.'; }

	public static function pageFormLogin_success():string { return 'You logged in successfully.'; }

}
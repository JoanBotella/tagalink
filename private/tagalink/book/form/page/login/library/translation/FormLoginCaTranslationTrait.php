<?php
declare(strict_types=1);

namespace tagalink\book\form\page\login\library\translation;

trait
	FormLoginCaTranslationTrait
{

	// --- pageFormLogin ---

	public static function pageFormLogin_segment():string { return 'entrar'; }

	public static function pageFormLogin_errorUserNotFound():string { return 'No existeix cap usuari amb eixa adreça d\'e-mail i contrasenya.'; }

	public static function pageFormLogin_success():string { return 'Has entrat exitosament.'; }

}
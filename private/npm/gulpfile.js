'use strict';

const
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	minifyJs = require('gulp-terser'),
	uglifyCss = require('gulp-csso'),
	del = require('del'),

	projectDir = '../..',

		privateDir = projectDir + '/private',

			tagalinkDir = privateDir + '/tagalink',

		publicDir = projectDir + '/public/asset',

			publicStyleDir = publicDir + '/style',
			publicImageDir = publicDir + '/image',
			publicScriptDir = publicDir + '/script'
;

const deleteDir = function (dir)
{
	del.sync(
		[
			dir + '/**',
		],
		{
			force: true
		}
	);
}

function style(done)
{
	deleteDir(publicStyleDir);

	gulp
		.src([
			tagalinkDir + '/asset/style/head.css',

			tagalinkDir + '/service/widget/html/asset/style/htmlWidgetStyle.css',

			tagalinkDir + '/book/front/service/widget/body/asset/style/frontBodyWidgetStyle.css',
			tagalinkDir + '/book/front/service/widget/footer/asset/style/frontFooterWidgetStyle.css',
			tagalinkDir + '/book/front/service/widget/header/asset/style/frontHeaderWidgetStyle.css',
			tagalinkDir + '/book/front/service/widget/nav/asset/style/frontNavWidgetStyle.css',
			tagalinkDir + '/book/front/service/widget/notifications/asset/style/frontNotificationsWidgetStyle.css',
			tagalinkDir + '/book/front/service/widget/systemLinks/asset/style/frontSystemLinksWidgetStyle.css',

			tagalinkDir + '/book/front/page/home/asset/style/frontHomePageStyle.css',
		])
		.pipe(concat('tagalink.css'))
		.pipe(gulp.dest(publicStyleDir))
		.pipe(rename('tagalink.min.css'))
		.pipe(uglifyCss())
		.pipe(gulp.dest(publicStyleDir))
	;

	done();
}

function image(done)
{
	deleteDir(publicImageDir);

	gulp
		.src([
			tagalinkDir + '/asset/image/**',
		])
		.pipe(gulp.dest(publicImageDir))
	;

	done();
}

function script(done)
{
	deleteDir(publicScriptDir);

	gulp
		.src([
			tagalinkDir + '/asset/script/head.js',

			tagalinkDir + '/book/front/asset/script/frontBookScript.js',

			tagalinkDir + '/book/front/page/home/asset/script/frontHomePageScript.js',

			tagalinkDir + '/asset/script/route.js',
			tagalinkDir + '/asset/script/tail.js',
		])
		.pipe(concat('tagalink.js'))
		.pipe(gulp.dest(publicScriptDir))
		.pipe(rename('tagalink.min.js'))
		.pipe(minifyJs())
		.pipe(gulp.dest(publicScriptDir))
	;

	done();
}

exports.default = gulp.parallel(
	style,
	image,
	script
);
